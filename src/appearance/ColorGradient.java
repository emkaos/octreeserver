package appearance;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.vecmath.Color3f;

/**
 * This class represents a color gradient that is used for picking the colors in which to display detail level 3 cells.
 * 
 * @author mkoch
 *
 */
public class ColorGradient {
	
	private BufferedImage image;
	private int width;
	
	/**
	 * Constructs the class from the source PGN file  with the gradient.
	 * 
	 * @param sourcefile
	 * @throws IOException
	 */
	public ColorGradient(File sourcefile) throws IOException {
		image = ImageIO.read(sourcefile);
		this.width = image.getWidth() -1;
	}
	
	/**
	 * Takes a value from 0 to 1 and returns the corresponding color value from the gradient.
	 * 
	 * @param scale
	 * @return
	 */
	public Color3f getColor(float scale) {
		if (scale > 1) scale = 1;
		if (scale < 0) scale = 0;
		int index = (int)(width * scale);
		//System.out.println(scale + ":" + index);
		int pixel = image.getRGB(index, 0);
		int alpha = (pixel >> 24) & 0xff;
	    int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    //System.out.println(red +"," + green + "," + blue);
		Color3f color = new Color3f(red/255f, green/255f, blue/255f);
		return color;
	}
	
}
