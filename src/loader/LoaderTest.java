package loader;

import java.io.File;

import octree.Octree;
import octree.OctreeCell;

public class LoaderTest {
	
	public static int n=0;
	public static int e=0;
	
	public static void main(String[] args) {
		FileLoader loader = new FileLoader(new File("/Users/matti/ma/octree/octree/wikilinks50k.coords.txt"));
		Octree tree = loader.getTree(5);
		printTree(tree);
	}
	
	public static void printTree(Octree tree) {
		n = 0;
		e = 0;
		printCell(tree.root);
		System.out.println("Alltogether " + n + " nodes.");
		System.out.println("Alltogether " + e + " edges.");
	}
	
	public static void printCell(OctreeCell cell) {
		System.out.println("Nodecount:" + cell.getNodeCount());
		n += cell.getNodeCount();
		e += cell.getEdgeCount();
		if (cell.children != null) {
			for (OctreeCell child : cell.children) {
				printCell(child);
			}
		}
	}
}
