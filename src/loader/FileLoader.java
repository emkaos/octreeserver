package loader;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.vecmath.Vector3f;

import appearance.ColorGradient;

import octree.Edge;
import octree.Node;
import octree.Octree;
import octree.json.JSONCache;

/**
 * Class to load the sourcefiles and create the octree.
 * 
 * @author mkoch
 */
public class FileLoader {
	
	File sourcefile;
	
	/**
	 * Constructor to set the main graph source file
	 * 
	 * @param file
	 */
	public FileLoader(File file) {
		this.sourcefile = file;
	}
	
	/**
	 * Creates the octree by loading the source files, creating the tree and adding all nodes and edges to the tree. Also takes parameters from the config files.
	 * 
	 * @param depths
	 * @param scalefactor
	 * @param gradient
	 * @param max_cell_nodes
	 * @param max_cell_edges
	 * @param max_transparency
	 * @param average_transparency
	 * @param node_size
	 * @return
	 * 
	 */
	public Octree getTree(int depths, float scalefactor, ColorGradient gradient, int max_cell_nodes, int max_cell_edges, float max_transparency, float average_transparency, float node_size) {
		  float minx = Float.MAX_VALUE;
		  float miny = Float.MAX_VALUE;
		  float minz = Float.MAX_VALUE;
		  float maxx = Float.MIN_VALUE;
		  float maxy = Float.MIN_VALUE;
		  float maxz = Float.MIN_VALUE;
		  int nodecount = 0;
		  int edgecount = 0;
		  
		  HashMap<String, Node> nodemap = new HashMap<String, Node>();
		  HashMap<String, String[]> edgemap = new HashMap<String, String[]>();
		
		  FileInputStream fstream;
		  try {
			  fstream = new FileInputStream(sourcefile);
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			  String line;
			  
			  System.out.println("Loading file...");
			  
			  while ((line = br.readLine()) != null)   {
				  line = line.trim();
				  //System.out.println(line);
				  String[] tokens = line.split("\t");
				  String id = tokens[0].trim();
				  String name = tokens[1].trim();
				  String[] vars = tokens[2].split(",");
				  if (vars.length < 3) System.out.println(tokens[0]);
				  float x = Float.parseFloat(vars[0]);
				  float y = Float.parseFloat(vars[1]);
				  float z = Float.parseFloat(vars[2]);
				  
				  if (x < minx) minx = x;
				  if (x > maxx) maxx = x;
				  if (y < miny) miny = y;
				  if (y > maxy) maxy = y;
				  if (z < minz) minz = z;
				  if (z > maxz) maxz = z;
				  
				  // save node
				  Node n = new Node(new Vector3f(x,y,z), id, name, node_size);
				  nodemap.put(id, n);
				  
				  if (tokens.length > 2) {
					  String[] target_ids = new String[tokens.length-3];
					  for (int i = 3; i < tokens.length; i++ ) {
						  target_ids[i-3] = tokens[i].trim();
					  }
					  edgemap.put(id, target_ids);
				  } else {
					  edgemap.put(id, null);
				  }
			  }	
			  
			  // add edges to nodes for pathfinding
			  for (Entry<String, String[]> entry : edgemap.entrySet()) {
				  Node source = nodemap.get(entry.getKey());
				  String[] target_ids = entry.getValue();
				  if (target_ids != null) {
					  Node[] targets = new Node[target_ids.length];
					  int i = 0;
					  for (String id : target_ids) {
						 targets[i] = nodemap.get(id); 
						 if (targets[i].sources == null) {
							 targets[i].sources = new ArrayList<Node>();
						 };
						 targets[i].sources.add(source);
						 i++; 
					  }
					  source.targets = targets;
					  
					  
				  }
			  }
			  
			  System.out.println("File loaded. Calculating geometrical adjustment...");
			  
			  // scaling and translating graph
			  float scale_target = nodemap.size() * scalefactor;
			  System.out.println("Scale target:" + scale_target);
			  float xmul = scale_target / (maxx - minx);
			  float ymul = scale_target / (maxy - miny);
			  float zmul = scale_target / (maxz - minz);
			  
			  float mul = Math.min(xmul, Math.max(ymul, zmul));
			  
			  
			  float xtrans = -((maxx-minx)*mul/2f) - minx*mul;
			  float ytrans = -((maxy-miny)*mul/2f) - miny*mul;
			  float ztrans = -((maxz-minz)*mul/2f) - minz*mul;
			  
			  
			  minx = minx*mul + xtrans;
			  miny = miny*mul + ytrans;
			  minz = minz*mul + ztrans;
			  maxx = maxx*mul + xtrans;
			  maxy = maxy*mul + ytrans;
			  maxz = maxz*mul + ztrans;
			  
			  for (String key : nodemap.keySet()) {
				  Node node = nodemap.get(key);
				  
				  // scale and translate
				  node.center = new Vector3f(node.center.x*mul+xtrans, node.center.y*mul+ytrans, node.center.z*mul+ztrans);
				  nodemap.put(key, node);
			  }
			  System.out.println("Adjusted Graph to (" + minx + ", " + miny + ", " + minz + ") - (" + maxx + ", " + maxy + ", " + maxz + ")");
			  System.out.println("Now building Tree...");
			  
			  
			  // now build tree
			  Octree tree = new Octree(minx, maxx, miny, maxy, minz, maxz, depths, gradient, max_cell_nodes, max_cell_edges, max_transparency, average_transparency);
			  
			  for (String key : nodemap.keySet()) {
				  Node node = nodemap.get(key);
				  
				  nodecount++;
				  tree.insertNode(node);
				  if (edgemap.get(key) != null) {
					  for (String target : edgemap.get(key)) {
						  edgecount++;
						  Node target_node = nodemap.get(target);
						  Edge edge = new Edge(node, target_node);
						  tree.insertEdge(edge);
					  }
				  }
			  }
			  System.out.println("Tree built with " + nodecount + " nodes and " + edgecount + " edges.");
			  tree.calculateVisibleBounds();
			  tree.buildTreeStatistics();
			  tree.statistics.printStatistics();
			  return tree;
			  
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
