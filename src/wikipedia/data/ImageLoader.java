package wikipedia.data;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.vecmath.Vector3f;

import octree.Edge;
import octree.Node;
import octree.Octree;
import octree.json.JSONCache;
import search.OctreeIndex;

/**
 * DatapPovider for Image URLS.
 * 
 * @author mkoch
 *
 */
public class ImageLoader {
	
	File sourcefile;
	
	/**
	 * Constructs and sets the source file.
	 * 
	 * @param file
	 */
	public ImageLoader(File file) {
		this.sourcefile = file;
	}
	
	/**
	 * Loads the data and returns a ImageProvider with the loaded data.
	 * 
	 * @param index
	 * @return
	 */
	public ImageProvider getImageprovider(OctreeIndex index) {
		  
		  HashMap<String, String> map = new HashMap<String, String>();
		
		  FileInputStream fstream;
		  try {
			  fstream = new FileInputStream(sourcefile);
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			  String line;
			  while ((line = br.readLine()) != null)   {
				  line = line.trim();
				  //System.out.println(line);
				  String[] tokens = line.split("\t");
				  String name = tokens[0].trim();
				  String image = tokens[1].trim();
				  Node node = index.findNodeByName(name);
				  if (node != null) {
					  map.put(node.id, image);
				  }
			  }	
			  System.out.println("Images loaded for " + map.size() + " nodes.");
			  ImageProvider provider = new ImageProvider(map);
			  return provider;
			  
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
