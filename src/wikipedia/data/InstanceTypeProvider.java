package wikipedia.data;

import java.util.Map;

import octree.Node;
import data.DataProvider;

public class InstanceTypeProvider implements DataProvider {
	
	private Map<String, String> id_image_map;
	
	public InstanceTypeProvider() {
		
	}
	
	public void setImageData(Map<String, String> id_image_map) {
		this.id_image_map = id_image_map;
	}
	
	@Override
	public String getDataForNode(Node node) {
		String filename = id_image_map.get(node.id); 
		if (filename == null) {
			return "";
		} else {
			return filename;
		}
	}

}
