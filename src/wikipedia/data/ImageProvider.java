package wikipedia.data;

import java.util.HashMap;
import java.util.Map;

import octree.Node;
import data.DataProvider;

/**
 * DataProvider for image urls.
 * 
 * @author mkoich
 *
 */
public class ImageProvider implements DataProvider {
	
	private Map<String, String> id_image_map;
	
	/**
	 * Sets up empty map. 
	 */
	public ImageProvider() {
		id_image_map = new HashMap<String, String>();
	}
	
	/**
	 * Sets up with filled map.
	 * 
	 * @param id_image_map
	 */
	public ImageProvider(Map<String, String> id_image_map) {
		this.id_image_map = id_image_map;
	}
	
	/**
	 * Sets the <nodeif, image_url> map
	 * 
	 * @param id_image_map
	 */
	public void setImageData(Map<String, String> id_image_map) {
		this.id_image_map = id_image_map;
	}
	
	/* (non-Javadoc)
	 * @see data.DataProvider#getDataForNode(octree.Node)
	 */
	@Override
	public String getDataForNode(Node node) {
		String filename = id_image_map.get(node.id); 
		if (filename == null) {
			return "\"\"";
		} else {
			return "\"" + filename + "\"";
		}
	}

}
