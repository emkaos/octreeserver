package wikipedia;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.vecmath.Color3f;

import octree.Edge;
import octree.Node;
import octree.Octree;
import octree.OctreeCell;
import octree.json.JSONCache;
import octree.json.JSONGenerator;
import wikipedia.data.ImageProvider;


/**
 * Wikipedia specific JSONGenerator
 * 
 * @author mkoch
 *
 */
public class WikipediaGenerator extends JSONGenerator{
	
	private ImageProvider image_provider;
	
	/**
	 * Creates the generator with a DataProvider for images (ImageProvider)
	 * 
	 * @param tree
	 * @param image_provider
	 */
	public WikipediaGenerator(Octree tree, ImageProvider image_provider) {
		super();
		this.image_provider = image_provider;
		//cache = new JSONCache(this, tree);
	}
	
	/* FORMAT
	 * DETAIL2:
	 * [[nodeid1, nodex1. nodey1, nodez1, nodeimage, nodeid2, nodec2 ...], [edge1_source_x, edge1_source_y, edge1_source_z, edge1_target_x, edge1_target_y, edge1_target_z, edge2_source_x, ...]]
	 * 
	 * DETAIL2:
	 * [[nodeid1, nodex1. nodey1, nodez1, nodeid2, nodec2 ...], [edge1_source_x, edge1_source_y, edge1_source_z, edge1_target_x, edge1_target_y, edge1_target_z, edge2_source_x, ...]]
	 * 
	 * DETAIL3:
	 * [centerx, centery, centerz, sizex, sizey, sizez, color_r, color_g, color_b]
	 */
	
	/* (non-Javadoc)
	 * @see octree.json.JSONGenerator#cell2json(int, octree.OctreeCell)
	 */
	public String cell2json(int detail, OctreeCell cell) {
		if (cell.getNodeCount() > 0) {
			StringBuilder json = new StringBuilder("[");
			if (detail == 1) {
				// nodes
				json.append("\""+cell.id+"\"");
				json.append(",[");
				//System.out.println(cell.getNodeCount());
				for (Node node : cell.getNodes()) {
					json.append(node.id);
					json.append(',');
					json.append(CF.format(node.center.x));
					json.append(',');
					json.append(CF.format(node.center.y));
					json.append(',');
					json.append(CF.format(node.center.z));
					json.append(',');
					json.append(image_provider.getDataForNode(node));
					json.append(',');
				}
				
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append("],[");
				// edges
				
				for (Edge edge : cell.getEdges()) {
					json.append(CF.format(edge.source.center.x));
					json.append(',');
					json.append(CF.format(edge.source.center.y));
					json.append(',');
					json.append(CF.format(edge.source.center.z));
					json.append(',');
					json.append(CF.format(edge.target.center.x));
					json.append(',');
					json.append(CF.format(edge.target.center.y));
					json.append(',');
					json.append(CF.format(edge.target.center.z));
					json.append(',');
				}
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append(']');
			}
			if (detail == 2 ) {
				// nodes
				json.append("\""+cell.id+"\"");
				json.append(",[");
				//System.out.println(cell.getNodeCount());
				for (Node node : cell.getNodes()) {
					json.append(node.id);
					json.append(',');
					json.append(CF.format(node.center.x));
					json.append(',');
					json.append(CF.format(node.center.y));
					json.append(',');
					json.append(CF.format(node.center.z));
					json.append(',');
				}
				
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append("],[");
				// edges
				
				for (Edge edge : cell.getEdges()) {
					json.append(CF.format(edge.source.center.x));
					json.append(',');
					json.append(CF.format(edge.source.center.y));
					json.append(',');
					json.append(CF.format(edge.source.center.z));
					json.append(',');
					json.append(CF.format(edge.target.center.x));
					json.append(',');
					json.append(CF.format(edge.target.center.y));
					json.append(',');
					json.append(CF.format(edge.target.center.z));
					json.append(',');
				}
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append(']');
					
			} else if (detail == 3) {
				Color3f color = cell.getColor();
				json.append("\""+cell.id+"\"");
				json.append(',');
				json.append(CF.format(cell.visible_center.x));
				json.append(',');
				json.append(CF.format(cell.visible_center.y));
				json.append(',');
				json.append(CF.format(cell.visible_center.z));
				json.append(',');
				json.append(CF.format(cell.visible_sizex));
				json.append(',');
				json.append(CF.format(cell.visible_sizey));
				json.append(',');
				json.append(CF.format(cell.visible_sizez));
				json.append(',');
				json.append(color.x);
				json.append(',');
				json.append(color.y);
				json.append(',');
				json.append(color.z);
				json.append(',');
				json.append(cell.getTransparency());
			//	System.out.println(cell.getTransparency());
				//json.append(',');
			}
			
			json.append("]");
			return json.toString();
		} else {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see octree.json.JSONGenerator#node2Json(octree.Node)
	 */
	public String node2Json(Node node) {
		StringBuffer json = new StringBuffer("[");
		json.append(node.id);
		json.append(',');
		json.append(CF.format(node.center.x));
		json.append(',');
		json.append(CF.format(node.center.y));
		json.append(',');
		json.append(CF.format(node.center.z));
		json.append(',');
		json.append('\"' + node.name + '\"');
		json.append(']');
		return json.toString();
	}
	
}
