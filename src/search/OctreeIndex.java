package search;

import java.util.ArrayList;
import java.util.HashMap;

import octree.Node;
import octree.Octree;
import octree.OctreeCell;

/**
 * Index for seaching nodes by name and id.
 * 
 * @author mkoch
 *
 */
public class OctreeIndex {
	private Octree tree;
	private HashMap<String, Node> name_map;
	private HashMap<String, Node> id_map;
	
	private SearchIndex searchindex;
	
	/**
	 * Sets up empty index from a tree.
	 * 
	 * @param tree
	 */
	public OctreeIndex(Octree tree) {
		this.tree = tree;
		this.searchindex = new SearchIndex();
	}
	
	/**
	 * Fills the index with the data from a tree.
	 */
	public void indexOctree() {
		name_map = new HashMap<String, Node>();
		id_map = new HashMap<String, Node>();
		walkTree(tree.root);
		this.searchindex.finishIndex();
		System.out.println("Finished tree search index");
	}
	
	/**
	 * Walks the tree to fill the index.
	 * 
	 * @param cell
	 */
	private void walkTree(OctreeCell cell) {
		if (cell.children != null) {
			for (OctreeCell child : cell.children) {
				walkTree(child);
			} 
		} else {
			for (Node node : cell.getNodes()) {
				String name = node.name;
				String id = node.id;
				name = name.trim().toLowerCase();
				name_map.put(name, node);
				id_map.put(id, node);
				searchindex.addNode(node);
			}
		}
	}
	
	/**
	 * Find a node by his name.
	 * 
	 * @param name
	 * @return
	 */
	public Node findNodeByName(String name) {
		name = name.trim().toLowerCase();
		Node node = name_map.get(name);
		return node;
	}
	
	/**
	 * Find a node by his id.
	 * 
	 * @param id
	 * @return
	 */
	public Node findNodeById(String id) {
		id = id.trim().toLowerCase();
		Node node = id_map.get(id);
		return node;
	}
	
	/**
	 * Searches for a node with a help of SearchIndex.
	 * 
	 * @param name
	 * @return
	 */
	public ArrayList<Node> searchNodes(String name) {
		ArrayList<String> names = searchindex.search(name);
		ArrayList<Node> results = new ArrayList<Node>();
		for (String resultname : names) {
			Node node = findNodeByName(resultname);
			results.add(node);
		}
		return results;
	}
}