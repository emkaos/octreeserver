package search;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import octree.Node;

/**
 * Search index for the octree to perform text search.
 * 
 * @author mkoch
 *
 */
public class SearchIndex {

	private StringBuffer indexbuffer;
	private String index = "";
	
	/**
	 * Constructs an empty index.  
	 */
	public SearchIndex() {
		indexbuffer = new StringBuffer("\t");
	}

	/**
	 * Adds a node to the index.
	 * 
	 * @param node
	 */
	public void addNode(Node node) {
		indexbuffer.append(node.name);
		indexbuffer.append("\t");
	};
	
	/**
	 * Prepares the index for searching. 
	 */
	public void finishIndex() {
		index = indexbuffer.toString();
		indexbuffer = new StringBuffer();
	}
	
	/**
	 * Searches the index for nodes which names include the searchtext.
	 * 
	 * @param searchtext
	 * @return
	 */
	public ArrayList<String> search(String searchtext) {
		System.out.println("SEARCHING FOR " + searchtext);
		ArrayList<String> results = new ArrayList<String>();
		Pattern p = Pattern.compile("\t([^\t]*"+searchtext+"[^\t]*)\t", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(index);
		while(m.find()) {
			String name = m.group(1); 
			results.add(name);
		}
		return results;
	}
	
}
