package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;


public class PageLinksNT2 {
	private final static int MAXNODES = 1600000;
	
	static String idsourcefile = "/Users/matti/MA/data/short_abstracts_de.nt";
	static String outfile = "/Users/matti/MA/data/page_links.lgl";
	static String linksourcefile = "/Volumes/Huge k machine/MA/page_links_de.nt";
	
	public static HashMap<String, Integer> loadIds() {
		
		HashMap<String, Integer> id_map = new HashMap<String, Integer>(MAXNODES);
		int id = 0;
		
		FileInputStream fstream;
		try {
			fstream = new FileInputStream(idsourcefile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream, "UTF-8"), 100000);
			String line;
			
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					int e = line.indexOf('>', 32);
					String name = line.substring(32, e);
					id_map.put(name, id);
					id++;
				}
			}
			
			br.close();
			fstream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return id_map;
	}
	
	public static HashMap<Integer, ArrayList<Integer>> loadLinks(HashMap<String, Integer> id_map) {
		HashMap<Integer, ArrayList<Integer>> link_map = new HashMap<Integer, ArrayList<Integer>>(MAXNODES);
		
		int counter = 0;
		
		FileInputStream fstream;
		try {
			fstream = new FileInputStream(linksourcefile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream, "UTF-8"), 100000);
			String line;
			
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					counter++;
					int e = line.indexOf('>', 32);
					String source = line.substring(32, e);
				
					if (!id_map.containsKey(source)) {
						continue;
					}
					
					e += 81;
					int f = line.indexOf('>', e);
					String target = line.substring(e,f);
					
					if (!id_map.containsKey(target)) {
						continue;
					}
					
					Integer source_id = id_map.get(source);
					Integer target_id = id_map.get(target);
					
					if (link_map.containsKey(target_id)) {
						ArrayList<Integer> target_links = link_map.get(target_id);
						if (target_links.contains(source_id)) {
							continue;
						}
						// no circular links
					}
					ArrayList<Integer> source_links;
					if (link_map.containsKey(source_id)) {
						source_links = link_map.get(source_id);
					} else {
						source_links = new ArrayList<Integer>();
					}
					
					source_links.add(target_id);
					link_map.put(source_id, source_links);
					
					counter++;
					if (counter % 100000 == 0) {
						System.out.println(counter);
					}
				}
			}
			
			br.close();
			fstream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return link_map;
	}
	
	public static void main(String args[]) {
		// load abstract nodes to generate page ids.
		
		HashMap<String, Integer> id_map = loadIds();
		HashMap<Integer, ArrayList<Integer>> link_map = loadLinks(id_map);
		
		// write
		int i = 0;
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(outfile), "UTF-8"));
			  for (Integer id : id_map.values()) {
			  out.append("# ");
			  out.append(id.toString());
			  out.write("\n");
			  ArrayList<Integer> ids = link_map.get(id);
			  if (ids != null) {
				  for (Integer lid : ids) {
					  out.write(lid.toString());
					  out.write("\n");
				  }
			  }
			  if (i++ % 10000 == 0){
				  System.out.println("Building lglfile: reached node " + i);
			  }
			  }
			out.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
