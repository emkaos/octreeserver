package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;


public class PageIDs {
	private final static int MAXNODES = 1600000;
	
	static String sourcefile = "/Users/matti/MA/data/short_abstracts_de.nt";
	static String outfile = "/Users/matti/MA/data/page_ids.txt";
	
	public static void main(String args[]) {
		// load abstract nodes to generate page ids.
		
		HashMap<String, Integer> id_map = new HashMap<String, Integer>(MAXNODES);
		int id = 0;
		
		FileInputStream fstream;
		try {
			fstream = new FileInputStream(sourcefile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream, "UTF-8"), 100000);
			String line;
			
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					int e = line.indexOf('>', 32);
					String name = line.substring(32, e);
					id_map.put(name, id);
					id++;
				}
			}
			
			br.close();
			fstream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// write
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(outfile), "UTF-8"));
			for (String key : id_map.keySet()) {
				StringBuffer s = new StringBuffer(key);
				s.append("<>");
				s.append(id_map.get(key));
				s.append("\n");
				out.write(s.toString());
			}
			out.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
