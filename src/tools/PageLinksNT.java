package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PageLinksNT {
	
	
	public static void main(String args[]) {
		  HashMap<String, Integer> id_map = new HashMap<String, Integer>();
		  HashMap<String, Vector<Integer>> link_map = new HashMap<String, Vector<Integer>>();
		  Vector<Integer> done = new Vector<Integer>();
	
		  String matchexp = "<http://de.dbpedia.org/(\\w+)/(.*)> <http://dbpedia.org/ontology/wikiPageWikiLink> <http://de.dbpedia.org/(\\w+)/(.*)> \\.";
		  Pattern pattern = Pattern.compile(matchexp);
		
		  File sourcefile = new File("/Users/matti/MA/page_links_de.nt");
		  FileInputStream fstream;

		  try {
			  fstream = new FileInputStream(sourcefile);
			  Writer out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream("page_links_de.txt"), "UTF-8"));
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			  String line;
			  int i = 0;
			  int id = 0;
			  int id_to = 0;
			  int id_from = 0;
			  Matcher matcher;
			  while ((line = br.readLine()) != null) {
				  matcher = pattern.matcher(line);
				  matcher.find();
				  //System.out.println(line);
				  //System.out.println(matcher.matches());
				  if (!matcher.matches() || !(matcher.groupCount() == 4) ) {
					  System.out.println("SKIPPED: " + line);
					  continue;
				  }
				  if (!matcher.group(1).equals("resource"))
					  System.out.println(matcher.group(1));
				  if (!matcher.group(3).equals("resource"))
					  System.out.println(matcher.group(3));
				  String from = matcher.group(2);
				  String to = matcher.group(4);
				  if (!id_map.containsKey(from)) {
					  id_map.put(from, id);
					  id++;
				  }
				  if (!link_map.containsKey(from)) {
					  link_map.put(from, new Vector<Integer>());
				  }
				  if (!id_map.containsKey(to)) {
					  id_map.put(to, id);
					  id++;
				  }
				  
				  // watch for circular links
				  id_to = id_map.get(to);
				  id_from = id_map.get(from);
				  if (!done.contains(id_to) || !link_map.containsKey(to) || !link_map.get(to).contains(id_from)) {
					  Vector<Integer> links = link_map.get(from);
					  links.add(id_to);
					  link_map.put(from, links);
					  done.add(id_from);
				  }
				  if (id % 1000 == 0) {
					  System.out.println("Loading file: reached node id " + id);
				  }
			  }
			  
			  i = 0;
			  for (String key : id_map.keySet()) {
				  out.append("#");
				  out.append(id_map.get(key).toString());
				  out.write("\n");
				  Vector<Integer> ids = link_map.get(key);
				  if (ids != null) {
					  for (Integer lid : ids) {
						  out.write(lid.toString());
						  out.write("\n");
					  }
				  }
				  out.write("\n");
				  if (i++ % 1000 == 0){
					  System.out.println("Building lglfile: reached node " + i);
				  }
			  }
			  out.close();
			  br.close();
		  } catch (FileNotFoundException e) {
			e.printStackTrace();
	      } catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
	}
}
