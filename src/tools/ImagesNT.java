package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;


public class ImagesNT {
	private final static int MAXNODES = 1600000;
	
	static String sourcefile = "/Users/mkoch/ma/data/images_de.nt";
	static String outfile = "/Users/mkoch/ma/data/images.txt";
	
	
	public static HashMap<String, ArrayList<String>> loadLinks() {
		HashMap<String, ArrayList<String>> type_map = new HashMap<String, ArrayList<String>>(MAXNODES);
		
		int counter = 0;
		
		FileInputStream fstream;
		try {
			fstream = new FileInputStream(sourcefile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream, "UTF-8"), 100000);
			String line;
			
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					counter++;
					if (!line.startsWith("<http://de.dbpedia.org/resource/")) {
						continue;
					}
					if (line.indexOf("200px-") != -1) {
						continue;
					}
					
					int e = line.indexOf('>', 32);
					String source = line.substring(32, e);
					
					e += 41;
					e = line.lastIndexOf('/');
					e++;
					int f = line.indexOf('>', e);
					while (line.charAt(e) == '<' || line.charAt(e) == ' ') {
						e++;
					}
					String target = line.substring(e,f);
					//target = target.replaceFirst("commons", "de");
					
					ArrayList<String> source_links;
					if (type_map.containsKey(source)) {
						source_links = type_map.get(source);
					} else {
						source_links = new ArrayList<String>();
					}
					
					if (!source_links.contains(target)) {
						source_links.add(target);
					}
					type_map.put(source, source_links);
					
					counter++;
					if (counter % 100000 == 0) {
						System.out.println(counter);
					}
				}
			}
			
			br.close();
			fstream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return type_map;
	}
	
	public static void main(String args[]) {
		// load abstract nodes to generate page ids.
		
		HashMap<String, ArrayList<String>> instance_map = loadLinks();
		
		// write
		int i = 0;
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(outfile), "UTF-8"));
			  for (String id : instance_map.keySet()) {
			  out.append(id + "\t");
			  ArrayList<String> ids = instance_map.get(id);
			  if (ids != null) {
				  StringBuffer buf = new StringBuffer("");
				  for (String lid : ids) {
					  buf.append(lid);
					  buf.append("\t");
				  }
				  if (buf.charAt(buf.length()-1) == '\t') {
					  buf.setLength(buf.length()-1);
				  }
				  out.write(buf.toString());
			  }
			  out.write("\n");
			  if (i++ % 10000 == 0){
				  System.out.println("Building instance type file: reached node " + i);
			  }
			  }
			out.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
