package path;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import octree.Node;

/**
 * Class to calculate paths between two nodes.
 * 
 * @author mkoch
 *
 */
public class PathFinder {
	
	/**
	 * Calculates and returns a path from the first node to the second.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static ArrayList<Node> findPath(Node source, Node target) {
		HashSet<Node> visited = new HashSet<Node>();
		HashMap<Node, Node> previous = new HashMap<Node, Node>();
		
		ArrayList<Node> queue = new ArrayList<Node>();
		ArrayList<Node> path = new ArrayList<Node>();
		
		Node current = source;
		visited.add(current);
		queue.add(current);
		
		System.out.println("Looking for path from "+ source.name+ " to "+target.name);
		while(!queue.isEmpty()) {
			current = queue.remove(0);
			//System.out.println(current.name);
			if (current.equals(target)) {
				break;
			} else {
				
				if (current.targets != null)
				for (Node child : current.targets) {
					if (!visited.contains(child)) {
						queue.add(child);
						visited.add(child);
						previous.put(child, current);
					}
				}
				
				if (current.sources != null)
				for (Node child : current.sources) {
					if (!visited.contains(child)) {
						queue.add(child);
						visited.add(child);
						previous.put(child, current);
					}
				}
			}
		}
		if (!current.equals(target)) {
			// failed, no existing path
			System.out.println("No path found");
			return null;
		}
		
		for (Node node = target; node != null; node = previous.get(node)) {
			path.add(0, node);
			System.out.println(node.name+"<-");
		}
	
		return path;
	}

}
