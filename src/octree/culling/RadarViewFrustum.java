package octree.culling;

import javax.vecmath.Vector3f;

import octree.OctreeCell;

/**
 * Class representing a radar view frustum.
 * 
 * @author mkoch
 */
public class RadarViewFrustum {
	
	float z_near;
	float z_far;
	float view_angle;
	
	Vector3f position = new Vector3f(0,0,0);
	Vector3f orientation = new Vector3f(0,0,1);
	
	double view_angle_half_tan;
	
	/**
	 * Constructs the frustum with the z_near, z_far and view_angle parameters.
	 * 
	 * @param z_near
	 * @param z_far
	 * @param view_angle
	 */
	public RadarViewFrustum(float z_near, float z_far, float view_angle ) {
		this.z_near = z_near;
		this.z_far = z_far;
		this.view_angle = view_angle;
		
		this.view_angle_half_tan = Math.tan(this.view_angle / 2d);
	}
	
	
	/**
	 * Updates the frustum with the new user coordinates.
	 * 
	 * @param position
	 * @param orientation
	 */
	public void update(Vector3f position, Vector3f orientation) {
		this.position = position;
		this.orientation = orientation;
		this.orientation.normalize();
	}
	
	/**
	 * Tests if a cell is visible.
	 * 
	 * @param cell
	 * @return
	 */
	public Visibility isVisible(OctreeCell cell) {
		// vector to cell center
		Vector3f v = new Vector3f(cell.visible_center);
		v.sub(this.position);
		
		// radius of cell
		float cell_radius = new Vector3f(cell.visible_sizex, cell.visible_sizey, cell.visible_sizez).length() / 2;
		
		// is position in cell?
		float v_length = v.length();
		if (this.z_near < v_length && v_length < cell_radius) {
			cell.last_viewing_distance = 0;
			return Visibility.PARTIAL;
		}
		
		// Distance on line of sight
		float z_distance = v.dot(this.orientation);
		
		// behind or in front of frustum?
		if (z_distance - cell_radius > this.z_far || z_distance + cell_radius < this.z_near) {
			return Visibility.NONE;
		}
		
		// outside of view radius ?
		float view_radius = (float) (z_distance * this.view_angle_half_tan);
		
		Vector3f p = new Vector3f(this.orientation); // point on line of sight 
		p.scale(z_distance);
		//p.add(this.position);
		
		p.sub(v);
		float xy_distance = p.length();
		
		if (xy_distance < view_radius + cell_radius) {
			Vector3f q = new Vector3f(position);
			q.sub(cell.visible_center);
			cell.last_viewing_distance = q.length() - cell_radius;
			return Visibility.PARTIAL;
		} else {
			return Visibility.NONE;
		}
		
	}
}
