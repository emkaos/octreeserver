package octree.culling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import octree.Node;
import octree.Octree;
import octree.OctreeCell;
import octree.json.JSONGenerator;
import path.PathFinder;

import server.ServerDebug;

/**
 * Class to cull the visible cells of the Cctree.
 * 
 * @author mkoch
 */
public class OctreeCuller {
	
	private static float z_near_cutoff = 0;
	private static float z_far_cutoff = 1000000;
	
	private Octree tree;
	
	private HashSet<OctreeCell> last_visible_cells_d1;
	private HashSet<OctreeCell> last_visible_cells_d2;
	private HashSet<OctreeCell> last_visible_cells_d3;
	private HashSet<OctreeCell> visible_cells_d1;
	private HashSet<OctreeCell> visible_cells_d2;
	private HashSet<OctreeCell> visible_cells_d3;
	private HashSet<Node> tomark_nodes;
	private HashSet<Node> tounmark_nodes; 
	
	private RadarViewFrustum view_frustum;
	
	private boolean debug_mode = false;
	private int s1_cutoff = 30; // 30
	private int s2_cutoff = 1000;  // 2000
	private int s3_cutoff = 5000; // 5000
	private int se1_cutoff = 10000; // edges
	private int se2_cutoff = 200000;  // edges
	
	private JSONGenerator generator;
	
	
	/**
	 * Constructs the class from the tree, the JSON generator and the settings from the properties config file.
	 * 
	 * @param tree
	 * @param view_angle
	 * @param generator
	 * @param s1_cutoff
	 * @param se1_cutoff
	 * @param s2_cutoff
	 * @param se2_cutoff
	 * @param s3_cutoff
	 * @param debug
	 */
	public OctreeCuller(Octree tree, float view_angle, JSONGenerator generator, int s1_cutoff, int se1_cutoff, int s2_cutoff, int se2_cutoff, int s3_cutoff, boolean debug)  {
		this.s1_cutoff = s1_cutoff;
		this.se1_cutoff = se1_cutoff;
		this.s2_cutoff = s2_cutoff;
		this.se2_cutoff = se2_cutoff;
		this.s3_cutoff = s3_cutoff;
		this.debug_mode = debug;
		this.view_frustum = new RadarViewFrustum(z_near_cutoff, z_far_cutoff, view_angle);
		
		this.generator = generator;
		this.tree = tree;
		
		this.last_visible_cells_d1 = new HashSet<OctreeCell>();
		this.last_visible_cells_d2 = new HashSet<OctreeCell>();
		this.last_visible_cells_d3 = new HashSet<OctreeCell>();
		this.tomark_nodes = new HashSet<Node>();
		this.tounmark_nodes = new HashSet<Node>();
		
	}
	
	/**
	 * Turn the Debug Mode on and off. If the debug mode is on, the grids for newly added and removes cells are included.
	 * 
	 * @param debug_mode
	 */
	public void setDebug(boolean debug_mode) {
		this.debug_mode = debug_mode;
	}
	
	/**
	 * Resets the culler in case of a new connection. Deletes saved data for last scene. 
	 */
	public void reset() {
		this.last_visible_cells_d1.clear();
		this.last_visible_cells_d2.clear();
		this.last_visible_cells_d3.clear();
		this.tomark_nodes.clear();
		this.tounmark_nodes.clear();
	}
	
	/**
	 * Sets a Node for marking.
	 * 
	 * @param node
	 */
	public void markNode(Node node) {
		this.tomark_nodes.add(node);
	}
	
	/**
	 * Sets a node for unmarking.
	 * 
	 * @param node
	 */
	public void unmarkNode(Node node) {
		this.tounmark_nodes.add(node);
	}
	
	/**
	 * Deprecated and unused. 
	 * 
	 * @param position
	 * @param view_axis
	 */
	private void cullVisibleCells(Vector3f position, Vector3f view_axis) {
		this.visible_cells_d1 = new HashSet<OctreeCell>();
		this.visible_cells_d2 = new HashSet<OctreeCell>();
		this.visible_cells_d3 = new HashSet<OctreeCell>();
		
		this.view_frustum.update(position, view_axis);
		
		int counter_s1 = 0;
		int counter_s2 = 0;
		int s1_cutoff = 1000;
		int s2_cutoff = 500;
		
		TreeSet<OctreeCell> queue = new TreeSet<OctreeCell>(new CellDistanceComparator());
		
		ServerDebug.startTimer("cullloop");
		// expand all
		ArrayList<OctreeCell> to_expand = new ArrayList<OctreeCell>();
		for (OctreeCell cell : tree.root.children) {
			if (this.view_frustum.isVisible(cell) != Visibility.NONE) {
				to_expand.add(cell);
			}
		}
		
		while(to_expand.size() > 0) {
			OctreeCell cell = to_expand.get(0);
			to_expand.remove(0);
			if (this.view_frustum.isVisible(cell) != Visibility.NONE) {
				if (cell.children == null) {
					queue.add(cell);
				} else {
					for (OctreeCell child : cell.children) {
						to_expand.add(child);
					}
				}
			}
		}
		
		while(queue.size() > 0) {
			if (counter_s1 < s1_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.getNodeCount() > 0) {
					visible_cells_d2.add(first);
					counter_s1 += first.getNodeCount();
				}
			} else if (counter_s2 < s2_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.getNodeCount() > 0) {
						visible_cells_d3.add(first);
						counter_s2 += 1;
				}
			} else {
				break;
			}
		}
		System.out.println("S1 Counter: " + counter_s1);
		System.out.println("S2 Counter: " + counter_s2);
		ServerDebug.stopTimer("cullloop");
	}
	
	/**
	 * Culls all visible octree cells and return a list of all visible cells.
	 * 
	 * @param position
	 * @param view_axis
	 */
	private void cullVisibleCells2(Vector3f position, Vector3f view_axis) {
		this.visible_cells_d1 = new HashSet<OctreeCell>();
		this.visible_cells_d2 = new HashSet<OctreeCell>();
		this.visible_cells_d3 = new HashSet<OctreeCell>();
		
		this.view_frustum.update(position, view_axis);
		
		int counter_s1 = 0;
		int counter_s2 = 0;
		int counter_s3 = 0;
		
		int counter_se1 = 0;
		int counter_se2 = 0;
		
		TreeSet<OctreeCell> queue = new TreeSet<OctreeCell>(new CellDistanceComparator());
		// keep always sorted
		for (OctreeCell cell : tree.root.children) {
			if (this.view_frustum.isVisible(cell) != Visibility.NONE) {
				queue.add(cell);
			}
		}
		
		// always next 2?
		ServerDebug.startTimer("cullloop");
		while(queue.size() > 0) {
			if (counter_s1 < s1_cutoff && counter_se1 < se1_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.children == null) {
					visible_cells_d1.add(first);
					counter_s1 += first.getNodeCount();
					counter_se1 += first.getEdgeCount();
				} else {
					for (OctreeCell cell : first.children) {
						if (cell.getNodeCount() > 0) 
							if (this.view_frustum.isVisible(cell) != Visibility.NONE) {
									queue.add(cell);
							}
					}
				}
			} else if (counter_s2 < s2_cutoff && counter_se2 < se2_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.children == null) {
					visible_cells_d2.add(first);
					counter_s2 += first.getNodeCount();
					counter_se2 += first.getEdgeCount();
				} else {
					for (OctreeCell cell : first.children) {
						if (cell.getNodeCount() > 0) 
							if (this.view_frustum.isVisible(cell) != Visibility.NONE) {
									queue.add(cell);
							}
					}
				}
			} else if (counter_s3 < s3_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.children == null) {
					visible_cells_d3.add(first);
					counter_s3 += 1;
				} else {
					for (OctreeCell cell : first.children) {
						if (cell.getNodeCount() > 0) 
							if (this.view_frustum.isVisible(cell) != Visibility.NONE) {
									queue.add(cell);
							}
					}
				}
			} else {
				break;
			}
		}
		//System.out.println("S1 Counter: " + counter_s1);
		//System.out.println("S2 Counter: " + counter_s2);
		ServerDebug.stopTimer("cullloop");
	}
	
	
	/**
	 * Get all connected nodes for one node, including all edges the node participates in. Returns a String of JSON lists.
	 * 
	 * @param node
	 * @return
	 */
	public String getNeighbors(Node node) {
		StringBuffer json = new StringBuffer("[");
		
		// node
		json.append(generator.node2Json(node));
		json.append(",[");
		
		// children
		System.out.println(node.targets.length);
		if (node.targets == null || node.targets.length == 0) {
			json.append("],[");
		} else {
			for (Node child : node.targets) {
				json.append(generator.node2Json(child));
				json.append(",");
			}
			json.setLength(json.length()-1);
			json.append("],[");
		}
		
		// parents
		if (node.sources == null || node.sources.size() == 0) {
			json.append("]");
		} else {
			for (Node parent : node.sources) {
				json.append(generator.node2Json(parent));
				json.append(",");
			}
			json.setLength(json.length()-1);
			json.append("]");
		}
		
		json.append("]");
		return json.toString();
	}
	
	/**
	 * Returns a JSON list with a path from the first node to the second.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public String getPath(Node from, Node to) {
		StringBuffer json = new StringBuffer("[");
		ArrayList<Node> path = PathFinder.findPath(from, to);
		
		for (Node node : path) {
			json.append(generator.node2Json(node));
			json.append(",");
		}
		if (json.charAt(json.length() -1) == ',') {
			json.setLength(json.length()-1);
		}
		json.append("]");
		
		return json.toString();
	}
	
	/**
	 * Returns a JSON String with a complete representation of the scene update. Includes all cells to add and delete. 
	 * 
	 * @param position
	 * @param view_axis
	 * @return
	 */
	public String getVisibleDifferenceJSON(Vector3f position, Vector3f view_axis) {
		cullVisibleCells2(position, view_axis);
		
		boolean rebuild_d1 = false; // don't send differences, instead rebuild whole
		boolean rebuild_d2 = false;
		boolean rebuild_d3 = false;
		
		// find differences
		ServerDebug.startTimer("find_differences");
	    
		// detail 1
		ArrayList<OctreeCell> to_add_d1 = new ArrayList<OctreeCell>();
		ArrayList<OctreeCell> to_remove_d1 = new ArrayList<OctreeCell>();
		
		
		for (OctreeCell cell : visible_cells_d1) {
			if (!last_visible_cells_d1.contains(cell)) {
				to_add_d1.add(cell);
			}
		}
		for (OctreeCell cell : last_visible_cells_d1) {
			if (!visible_cells_d1.contains(cell)) {
				to_remove_d1.add(cell);
			}
		}
		if (to_remove_d1.size() + to_add_d1.size() > visible_cells_d1.size()) {
			rebuild_d1 = true;
		}
		
		// detail 2
		ArrayList<OctreeCell> to_add_d2 = new ArrayList<OctreeCell>();
		ArrayList<OctreeCell> to_remove_d2 = new ArrayList<OctreeCell>();
		
		for (OctreeCell cell : visible_cells_d2) {
			if (!last_visible_cells_d2.contains(cell)) {
				to_add_d2.add(cell);
			}
		}
		for (OctreeCell cell : last_visible_cells_d2) {
			if (!visible_cells_d2.contains(cell)) {
				to_remove_d2.add(cell);
			}
		}
		if (to_remove_d2.size() + to_add_d2.size() >= visible_cells_d2.size()) {
			rebuild_d2 = true;
		}
		
		// detail 3
		HashSet<OctreeCell> to_add_d3 = new HashSet<OctreeCell>();
		HashSet<OctreeCell> to_remove_d3 = new HashSet<OctreeCell>();
		
		for (OctreeCell cell : visible_cells_d3) {
			if (!last_visible_cells_d3.contains(cell)) {
				to_add_d3.add(cell);
			}
		}
		for (OctreeCell cell : last_visible_cells_d3) {
			if (!visible_cells_d3.contains(cell)) {
				to_remove_d3.add(cell);
			}
		}
		if (to_remove_d3.size() + to_add_d3.size() >= visible_cells_d3.size()) {
			rebuild_d3 = true;
		}
		
		last_visible_cells_d1 = visible_cells_d1;
		last_visible_cells_d2 = visible_cells_d2;
		last_visible_cells_d3 = visible_cells_d3;
		
		
		ServerDebug.stopTimer("find_differences");
		// create json
		
		
		ServerDebug.startTimer("build_json");
		StringBuffer json = new StringBuffer("[[");
		// deletion
		// lvl 1
		if (rebuild_d1) {
			json.append("\"ALL\",");
		} else {
			json.append(generator.cellsRemove2json(to_remove_d1));
			json.append(",");
		}
		
		if (rebuild_d2) {
			json.append("\"ALL\",");
		} else {
			json.append(generator.cellsRemove2json(to_remove_d2));
			json.append(",");
		}
		if (rebuild_d3) {
			json.append("\"ALL\",");
		} else {
			json.append(generator.cellsRemove2json(to_remove_d3));
			json.append(",");
		}
		if (this.debug_mode) {
			json.append(generator.cellsDebugGridjson(to_remove_d2));
		} else {
			json.append("[]");
		}
		json.append(',');
		if (this.tounmark_nodes.size() > 0) {
			json.append(generator.nodesUnmark2json(tounmark_nodes));
		} else {
			json.append("[]");
		}
		json.append("],[");
		// adding
		if (!rebuild_d1) {
			json.append(generator.cellsAdd2json(1, to_add_d1));
		} else { 
			json.append(generator.cellsAdd2json(1, visible_cells_d1));
		}
		json.append(',');
		
		if (!rebuild_d2) {
			json.append(generator.cellsAdd2json(2, to_add_d2));
		//	System.out.println("REMOVE:"+ to_remove_d2.size() + " ADD:"+to_add_d2.size());
		} else {
			json.append(generator.cellsAdd2json(2, visible_cells_d2));
		//	System.out.println("REMOVE ALL ADD:"+visible_cells_d2.size());
		}
		json.append(',');
		if (!rebuild_d3) {
			json.append(generator.cellsAdd2json(3, to_add_d3));
		} else {
			json.append(generator.cellsAdd2json(3, visible_cells_d3));
		}
		json.append(',');
		if (this.debug_mode) {
			json.append(generator.cellsDebugGridjson(to_add_d2));
		} else {
			json.append("[]");
		}
		json.append(',');
		if (this.tomark_nodes.size() > 0) {
			json.append(generator.nodesMark2json(tomark_nodes));
		} else {
			json.append("[]");
		}
		json.append("]]");
		ServerDebug.stopTimer("build_json");
		
		// debug stuff
		/*
		if (!rebuild_d2) {
			System.out.println("REMOVES:");
			for (OctreeCell cell : to_remove_d2) {
				System.out.print(cell.last_viewing_distance + "  ");
			}
			System.out.println("\nADDS:");
			for (OctreeCell cell : to_add_d2) {
				System.out.print(cell.last_viewing_distance + "  ");
			}
		}
		*/
		// debug stuff
		this.tomark_nodes.clear();
		this.tounmark_nodes.clear();
		return json.toString();
		
	}
	
	/**
	 * Unused.
	 * 
	 * @param position
	 * @param view_axis
	 * @return
	 */
	public String getVisibleCellsJSON(Vector3f position, Vector3f view_axis) {
		cullVisibleCells(position, view_axis);
		StringBuffer json = new StringBuffer("[");
		json.append(generator.cellsAdd2json(1, visible_cells_d1));
		json.append(',');
		json.append(generator.cellsAdd2json(2, visible_cells_d2));
//		System.out.println(JSONGenerator.debug);
//		System.out.println(JSONGenerator.debug2);
//		System.out.println(JSONGenerator.debug3);
//		System.out.println(JSONGenerator.debug4);
		json.append(',');
		json.append(generator.cellsAdd2json(3, visible_cells_d3));
	//	System.out.println(visible_cells.get(1).size());
	//	System.out.println(visible_cells.get(2).size());
	//	System.out.println(visible_cells.get(3).size());
		json.append(']');
	//	System.out.println(json.length());
		return json.toString();
	}

}
