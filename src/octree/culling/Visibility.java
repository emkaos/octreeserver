package octree.culling;

public enum Visibility {
	FULL, PARTIAL, NONE;
}
