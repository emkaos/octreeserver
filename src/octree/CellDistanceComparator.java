package octree;

import java.util.Comparator;

public class CellDistanceComparator implements Comparator<OctreeCell> {

	public int compare2(OctreeCell o1, OctreeCell o2) {
		return Float.compare(o1.last_viewing_distance, o2.last_viewing_distance);
	}
	
	@Override
	public int compare(OctreeCell o1, OctreeCell o2) {
		if (o1.last_viewing_distance == o2.last_viewing_distance && o1 != o2) {
			return -1;
			//return Integer.valueOf(o1.hashCode()).compareTo(o2.hashCode());
		} else {
			return Float.compare(o1.last_viewing_distance, o2.last_viewing_distance);
		}
	}

}
