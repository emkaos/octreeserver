package octree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import server.ServerDebug;

public class OctreeCuller {
	
	static float LVL1_CUTOFF = 20000;
	static float LVL2_CUTOFF = 20000;
	static float LVL3_CUTOFF = 10000;
	static float LVL4_CUTOFF = 5000;
	static float LVL5_CUTOFF = 2000;
	
	private float cellsize_lvl1;
	private float cellsize_lvl2;
	private float cellsize_lvl3;
	private float cellsize_lvl4;
	private float cellsize_lvl5;
	
	private Octree tree;
	private double view_angle;
	private double view_angle_tan;
	private double view_angle_cos;

	
	private HashSet<OctreeCell> last_visible_cells_d1;
	private HashSet<OctreeCell> last_visible_cells_d2;
	private HashSet<OctreeCell> last_visible_cells_d3;
	private HashSet<OctreeCell> visible_cells_d1;
	private HashSet<OctreeCell> visible_cells_d2;
	private HashSet<OctreeCell> visible_cells_d3;
	
	
	private Vector3f position;
	private Vector3f view_axis;
	
	public OctreeCuller(Octree tree, double view_angle)  {
		this.tree = tree;
		this.view_angle = view_angle;
		this.view_angle_tan = Math.tan(view_angle/2);
		this.view_angle_cos = Math.cos(view_angle/2);
		this.last_visible_cells_d1 = new HashSet<OctreeCell>();
		this.last_visible_cells_d2 = new HashSet<OctreeCell>();
		this.last_visible_cells_d3 = new HashSet<OctreeCell>();
		
		// calculate cell sizes up to level 5
 		OctreeCell c1 = tree.root.children[0];
		cellsize_lvl1 = new Vector3f(c1.sizex, c1.sizey, c1.sizez).length();
		OctreeCell c2 = tree.root.children[0].children[0];
		cellsize_lvl2 = new Vector3f(c2.sizex, c2.sizey, c2.sizez).length();
		OctreeCell c3 = tree.root.children[0].children[0].children[0];
		cellsize_lvl3 = new Vector3f(c3.sizex, c3.sizey, c3.sizez).length();
		OctreeCell c4 = tree.root.children[0].children[0].children[0].children[0];
		cellsize_lvl4 = new Vector3f(c4.sizex, c4.sizey, c4.sizez).length();
		OctreeCell c5 = tree.root.children[0].children[0].children[0].children[0].children[0];
		cellsize_lvl5 = new Vector3f(c5.sizex, c5.sizey, c5.sizez).length();
		System.out.println(c5.sizex + ":" + c5.sizey + ":" + c5.sizez);
	}
	
	public void reset() {
		this.last_visible_cells_d1.clear();
		this.last_visible_cells_d2.clear();
		this.last_visible_cells_d3.clear();
	}
	
	private float calculateViewingDistance(OctreeCell cell) {
		ServerDebug.startTimer("calculate_distance");
		// returns NaN for not visible
		// or float for distance from viewer
		
		// calculate nearest point in viewing axis from cell center
		Vector3f s = new Vector3f(cell.sizex, cell.sizey, cell.sizez);
		Vector3f p = new Vector3f(cell.center);
		p.sub(position);
		float n = p.dot(view_axis) / view_axis.dot(view_axis); // /1 falls normalisiert? 
		
		Vector3f pn = new Vector3f(p);
		pn.normalize();
		float m = pn.dot(view_axis);
		//System.out.print(m + ',');
		if (m < this.view_angle_cos && p.length() > s.length()) {
		//	System.out.println("VAN" + m);
			ServerDebug.stopTimer("calculate_distance");
			return Float.NaN; // behind avatar
		}
		
		Vector3f o =  new Vector3f(position);
		Vector3f a = new Vector3f(view_axis);
		a.scale(n);
		o.add(a);
		o.sub(cell.center);
		//System.out.println(n * view_angle_tan);
		//System.out.println(o.length());
		//System.out.println(s.length());
		ServerDebug.stopTimer("calculate_distance");
		return n;
		/*
		if (o.length() - s.length() < n * view_angle_tan) {
			return n;
		} 
		return Float.NaN;
		*/
	}
	
	
	private void insertSorted(ArrayList<OctreeCell> list, OctreeCell item) {
		ServerDebug.startTimer("queue_insert");
		int i = 0;
		boolean inserted = false;
		while (i < list.size()) {
			if (list.get(i).last_viewing_distance > item.last_viewing_distance) {
				list.add(i, item);
				inserted = true;
				break;
			}	
			i++;
		}
		if (!inserted) {
			list.add(item);
		}
		ServerDebug.stopTimer("queue_insert");
	}
	
	private void cullVisibleCells2(Vector3f position, Vector3f view_axis) {
		this.visible_cells_d1 = new HashSet<OctreeCell>();
		this.visible_cells_d2 = new HashSet<OctreeCell>();
		this.visible_cells_d3 = new HashSet<OctreeCell>();
		
		view_axis.normalize();
		this.position = position;
		this.view_axis = view_axis;
		// erstmal nur d2.
		// d3 in mehrere levels
		
		int counter_s1 = 0;
		int counter_s2 = 0;
		int s1_cutoff = 1000;
		int s2_cutoff = 500;
		TreeSet<OctreeCell> queue = new TreeSet<OctreeCell>(new CellDistanceComparator());
		// keep always sorted
		for (OctreeCell cell : tree.root.children) {
			float d = calculateViewingDistance(cell);
			cell.last_viewing_distance = d;
			if (d != Float.NaN) {
				queue.add(cell);
			}
		}
		
		// always next 2?
		ServerDebug.startTimer("cullloop");
		while(queue.size() > 0) {
			if (counter_s1 < s1_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.children == null) {
					if (first.getNodeCount() > 0) {
						visible_cells_d2.add(first);
						counter_s1 += first.getNodeCount();
					}
				} else {
					for (OctreeCell cell : first.children) {
						float d = calculateViewingDistance(cell);
						cell.last_viewing_distance = d;
						if (d != Float.NaN) {
							queue.add(cell);
						}
					}
				}
			} else if (counter_s2 < s2_cutoff) {
				OctreeCell first = queue.first();
				queue.remove(first);
				if (first.children == null) {
					if (first.getNodeCount() > 0) {
						visible_cells_d3.add(first);
						counter_s2 += 1;
					}
				} else {
					for (OctreeCell cell : first.children) {
						float d = calculateViewingDistance(cell);
						cell.last_viewing_distance = d;
						if (d != Float.NaN) {
							queue.add(cell);
						}
					}
				}
			} else {
				break;
			}
		}
		ServerDebug.stopTimer("cullloop");
	}
	
	
	private void cullVisibleCells(Vector3f position, Vector3f view_axis) {
		this.visible_cells_d1.clear();
		this.visible_cells_d2.clear();
		this.visible_cells_d3.clear();
		
		view_axis.normalize();
		this.position = position;
		this.view_axis = view_axis;
		
		// walk through all tree levels
		for (OctreeCell c1 : tree.root.children) {
			float d1 = calculateViewingDistance(c1);
			
			 // somewhat visible?
			if ( d1 != Float.NaN && d1 - cellsize_lvl1 < this.LVL1_CUTOFF ) {
				for (OctreeCell c2 : c1.children) {
					float d2 = calculateViewingDistance(c2);
					if ( d2 != Float.NaN && d2 - cellsize_lvl2 < this.LVL2_CUTOFF ) {
						for (OctreeCell c3 : c2.children) {
							float d3 = calculateViewingDistance(c3);
							if (d3 != Float.NaN && d3 - cellsize_lvl3 < this.LVL3_CUTOFF ) {
								for (OctreeCell c4 : c3.children) {
									float d4 = calculateViewingDistance(c4);
									if (d4 != Float.NaN && d4 - cellsize_lvl4 < this.LVL4_CUTOFF) {
										for (OctreeCell c5 : c4.children) {
											float d5 = calculateViewingDistance(c5);
											if (d5 != Float.NaN && c5.getNodeCount() > 0) {
												if (d5 - cellsize_lvl5 < this.LVL5_CUTOFF) {
														visible_cells_d2.add(c5);
												} else {
														visible_cells_d3.add(c5);
												}
											}
										}
										
									} else if (d4 != Float.NaN && d4 - cellsize_lvl4 < LVL3_CUTOFF) {
										if (c4.getNodeCount() > 0)
											visible_cells_d3.add(c4);
									}
								}
							} else if (d3 != Float.NaN  && d3 - cellsize_lvl3 < LVL2_CUTOFF) {
								if (c3.getNodeCount() > 0)
									visible_cells_d3.add(c3);
							}
						}
					} else if (d2 != Float.NaN && d2 - cellsize_lvl2 < LVL1_CUTOFF ) {
							if (c2.getNodeCount() > 0)
								visible_cells_d3.add(c2);
					}
				}
			}
			
		}
		
	}
	
	public String getVisibleDifferenceJSON(Vector3f position, Vector3f view_axis) {
		cullVisibleCells2(position, view_axis);
		
		// find differences
		
		// detail 1
		ServerDebug.startTimer("find_differences");
		ArrayList<OctreeCell> to_add_d1 = new ArrayList<OctreeCell>();
		ArrayList<OctreeCell> to_remove_d1 = new ArrayList<OctreeCell>();
		
		boolean rebuild_d1 = false; // don't send differences, instead rebuild whole
		boolean rebuild_d2 = false;
		boolean rebuild_d3 = false;
		
		for (OctreeCell cell : visible_cells_d1) {
			if (!last_visible_cells_d1.contains(cell)) {
				to_add_d1.add(cell);
			}
		}
		for (OctreeCell cell : last_visible_cells_d1) {
			if (!visible_cells_d1.contains(cell)) {
				to_remove_d1.add(cell);
			}
		}
		if (to_remove_d1.size() + to_add_d1.size() > visible_cells_d1.size()) {
			rebuild_d1 = true;
		}
		
		// detail 2
		ArrayList<OctreeCell> to_add_d2 = new ArrayList<OctreeCell>();
		ArrayList<OctreeCell> to_remove_d2 = new ArrayList<OctreeCell>();
		
		for (OctreeCell cell : visible_cells_d2) {
			if (!last_visible_cells_d2.contains(cell)) {
				to_add_d2.add(cell);
			}
		}
		for (OctreeCell cell : last_visible_cells_d2) {
			if (!visible_cells_d2.contains(cell)) {
				to_remove_d2.add(cell);
			}
		}
		if (to_remove_d2.size() + to_add_d2.size() >= visible_cells_d2.size()) {
			rebuild_d2 = true;
		}
		
		// detail 3
		HashSet<OctreeCell> to_add_d3 = new HashSet<OctreeCell>();
		HashSet<OctreeCell> to_remove_d3 = new HashSet<OctreeCell>();
		
		for (OctreeCell cell : visible_cells_d3) {
			if (!last_visible_cells_d3.contains(cell)) {
				to_add_d3.add(cell);
			}
		}
		for (OctreeCell cell : last_visible_cells_d3) {
			if (!visible_cells_d3.contains(cell)) {
				to_remove_d3.add(cell);
			}
		}
		if (to_remove_d3.size() + to_add_d3.size() >= visible_cells_d3.size()) {
			rebuild_d3 = true;
		}
		
		last_visible_cells_d1 = visible_cells_d1;
		last_visible_cells_d2 = visible_cells_d2;
		last_visible_cells_d3 = visible_cells_d3;
		
		
		ServerDebug.stopTimer("find_differences");
		// create json
		
		
		ServerDebug.startTimer("build_json");
		StringBuffer json = new StringBuffer("[[");
		// deletion
		// lvl 1
		if (rebuild_d1) {
			json.append("\"ALL\",");
		} else {
			json.append(JSONGenerator.cellsRemove2json(to_remove_d1));
			json.append(",");
		}
		if (rebuild_d2) {
			json.append("\"ALL\",");
		} else {
			json.append(JSONGenerator.cellsRemove2json(to_remove_d2));
			json.append(",");
		}
		if (rebuild_d3) {
			json.append("\"ALL\"");
		} else {
			json.append(JSONGenerator.cellsRemove2json(to_remove_d3));
		}
		json.append("],[");
		if (!rebuild_d1) {
			json.append(JSONGenerator.cellsAdd2json(1, to_add_d1));
		} else { 
			json.append(JSONGenerator.cellsAdd2json(1, visible_cells_d1));
		}
		json.append(',');
		
		if (!rebuild_d2) {
			json.append(JSONGenerator.cellsAdd2json(2, to_add_d2));
		} else {
			json.append(JSONGenerator.cellsAdd2json(2, visible_cells_d2));
		}
		json.append(',');
		if (!rebuild_d3) {
			json.append(JSONGenerator.cellsAdd2json(3, to_add_d3));
			System.out.println("REMOVE:"+ to_remove_d3.size() + " ADD:"+to_add_d3.size());
		} else {
			json.append(JSONGenerator.cellsAdd2json(3, visible_cells_d3));
			System.out.println("REMOVE ALL ADD:"+visible_cells_d3.size());
		}
		json.append("]]");
		ServerDebug.stopTimer("build_json");
		return json.toString();
		
	}
	
	public String getVisibleCellsJSON(Vector3f position, Vector3f view_axis) {
		cullVisibleCells(position, view_axis);
		StringBuffer json = new StringBuffer("[");
		json.append(JSONGenerator.cellsAdd2json(1, visible_cells_d1));
		json.append(',');
		json.append(JSONGenerator.cellsAdd2json(2, visible_cells_d2));
//		System.out.println(JSONGenerator.debug);
//		System.out.println(JSONGenerator.debug2);
//		System.out.println(JSONGenerator.debug3);
//		System.out.println(JSONGenerator.debug4);
		json.append(',');
		json.append(JSONGenerator.cellsAdd2json(3, visible_cells_d3));
	//	System.out.println(visible_cells.get(1).size());
	//	System.out.println(visible_cells.get(2).size());
	//	System.out.println(visible_cells.get(3).size());
		json.append(']');
	//	System.out.println(json.length());
		return json.toString();
	}

}
