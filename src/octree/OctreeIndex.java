package octree;

import java.util.HashMap;

public class OctreeIndex {
	private Octree tree;
	private HashMap<String, Node> name_map;
	private HashMap<String, Node> id_map;
	
	public OctreeIndex(Octree tree) {
		this.tree = tree;
	}
	
	public void indexOctree() {
		name_map = new HashMap<String, Node>();
		id_map = new HashMap<String, Node>();
		walkTree(tree.root);
		System.out.println("Finished tree search index");
	}
	
	private void walkTree(OctreeCell cell) {
		if (cell.children != null) {
			for (OctreeCell child : cell.children) {
				walkTree(child);
			} 
		} else {
			for (Node node : cell.getNodes()) {
				String name = node.name;
				String id = node.id;
				name = name.trim().toLowerCase();
				name_map.put(name, node);
				id_map.put(id, node);
			}
		}
	}
	
	public Node findNodeByName(String name) {
		name = name.trim().toLowerCase();
		Node node = name_map.get(name);
		return node;
	}
	
	public Node findNodeById(String id) {
		id = id.trim().toLowerCase();
		Node node = id_map.get(id);
		return node;
	}
}
