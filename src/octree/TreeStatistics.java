package octree;

/**
 * Class to collect and save statistic for an Octree.
 * 
 * @author mkoch
 */
public class TreeStatistics {
	public float max_node_density = 0;
	public float min_node_density = 0;
	public float average_node_density = 0;
	public float max_edge_density = 0;
	public float min_edge_density = 0;
	public float average_edge_density = 0;
	public float average_node_count = 0;
	public float average_edge_count = 0;
	public float max_node_count = 0;
	public float max_edge_count = 0;
	public float average_size = 0;
	public float max_size = 0;
	
	private int cells_processed;
	
	/**
	 * Gathers all statistics and saves them in the TreeStatistics and the Cells itself.
	 * 
	 * @param tree
	 */
	public void gatherStatistics(Octree tree) {
		this.cells_processed = 0;
		this.max_node_density = 0;
		this.min_node_density = 1;
		this.average_node_density = 0;
		this.max_edge_density = 0;
		this.min_edge_density = 1;
		this.average_edge_density = 0;
		this.walkTree(tree.root);
		
		// this is not 100% correct, but gives better results:
		this.average_node_density = this.average_node_count / this.average_size;
		this.average_edge_density = this.average_edge_count / this.average_size;
	}
	
	/**
	 * Walks the tree and calculates the statictics
	 * 
	 * @param cell
	 */
	private void walkTree(OctreeCell cell) {
		if (cell.children == null) {
			int nodecount = cell.getNodeCount();
			int edgecount = cell.getEdgeCount();
			
			float visible_size = (cell.visible_sizex * cell.visible_sizey * cell.visible_sizez);
			float node_density = nodecount  / visible_size;
			float edge_density = edgecount  / visible_size;
			if (this.max_size < visible_size) this.max_size = visible_size;
			this.average_size = ((this.average_size * this.cells_processed) + visible_size) / (this.cells_processed + 1);
			
			if (this.max_node_count < nodecount) this.max_node_count = nodecount;
			if (this.max_edge_count < edgecount) this.max_edge_count = edgecount;
			this.average_node_count = ((this.average_node_count * this.cells_processed) + nodecount) / (this.cells_processed + 1);
			this.average_edge_count = ((this.average_edge_count * this.cells_processed) + edgecount) / (this.cells_processed + 1);
			
			if (this.max_node_density < node_density) this.max_node_density = node_density;
			if (this.max_edge_density < edge_density) this.max_edge_density = edge_density;
			
			this.cells_processed++;
		} else {
			for (OctreeCell child : cell.children ) {
				walkTree(child);
			}
		}
	}
	
	
	/**
	 * Prints all global staticstics to stdout. 
	 */
	public void printStatistics() {
		System.out.println("#### Octree Statistics #################");
		System.out.println("Max visible cell size: " + this.max_size);
		System.out.println("Max node density: " + this.max_node_density);
		System.out.println("Max edge density: " + this.max_edge_density);
		System.out.println("Max node count: " + this.max_node_count);
		System.out.println("Max edge count: " + this.max_edge_count);
		System.out.println("Average visible cell size: " + this.average_size);
		System.out.println("Average node density: " + this.average_node_density);
		System.out.println("Average edge density: " + this.average_edge_density);
		System.out.println("Average node count: " + this.average_node_count);
		System.out.println("Average edge count: " + this.average_edge_count);
		System.out.println("Leaf sectors: " + this.cells_processed);
		System.out.println("########################################");
	}
}
