package octree;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.vecmath.Color3f;

public class JSONGenerator {
	
	/* FORMAT
	 * DETAIL2:
	 * [[nodeid1, nodex1. nodey1, nodez1, nodeid2, nodec2 ...], [edge1_source_x, edge1_source_y, edge1_source_z, edge1_target_x, edge1_target_y, edge1_target_z, edge2_source_x, ...]]
	 * 
	 * DETAIL3:
	 * [centerx, centery, centerz, sizex, sizey, sizez, color_r, color_g, color_b]
	 */
	
	public static int debug;
	public static int debug2;
	public static int debug3;
	public static int debug4;
	public static DecimalFormat CF = new DecimalFormat("#.#");
	
	
	public static String cell2json(int detail, OctreeCell cell) {
		if (cell.getNodeCount() > 0) {
			StringBuffer json = new StringBuffer("[");
			if (detail == 1) {
				// pass for now XXX
			}
			if (detail == 2 ) {
				debug2 += cell.getNodeCount();
				debug4 += cell.getEdgeCount();
				// nodes
				json.append("\""+cell.id+"\"");
				json.append(",[");
				//System.out.println(cell.getNodeCount());
				for (Node node : cell.getNodes()) {
					debug++;
					json.append(node.id);
					json.append(',');
					json.append(CF.format(node.center.x));
					json.append(',');
					json.append(CF.format(node.center.y));
					json.append(',');
					json.append(CF.format(node.center.z));
					json.append(',');
				}
				
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append("],[");
				// edges
				
				for (Edge edge : cell.getEdges()) {
					debug3++;
					json.append(CF.format(edge.source.center.x));
					json.append(',');
					json.append(CF.format(edge.source.center.y));
					json.append(',');
					json.append(CF.format(edge.source.center.z));
					json.append(',');
					json.append(CF.format(edge.target.center.x));
					json.append(',');
					json.append(CF.format(edge.target.center.y));
					json.append(',');
					json.append(CF.format(edge.target.center.z));
					json.append(',');
				}
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append(']');
					
			} else if (detail == 3) {
				Color3f color = cell.getColor();
				json.append("\""+cell.id+"\"");
				json.append(',');
				json.append(CF.format(cell.visible_center.x));
				json.append(',');
				json.append(CF.format(cell.visible_center.y));
				json.append(',');
				json.append(CF.format(cell.visible_center.z));
				json.append(',');
				json.append(CF.format(cell.visible_sizex));
				json.append(',');
				json.append(CF.format(cell.visible_sizey));
				json.append(',');
				json.append(CF.format(cell.visible_sizez));
				json.append(',');
				json.append(color.x);
				json.append(',');
				json.append(color.y);
				json.append(',');
				json.append(color.z);
				json.append(',');
				json.append(cell.getTransparency());
			//	System.out.println(cell.getTransparency());
				//json.append(',');
			}
			
			json.append("]");
			return json.toString();
		} else {
			return null;
		}
	}
	
	public static String cellsRemove2json(Collection<OctreeCell> cells) {
		StringBuffer json = new StringBuffer("[");
		for (OctreeCell cell : cells) {
			json.append("\""+cell.id+"\"");
			json.append(",");
		}
		if (json.charAt(json.length() -1) == ',') {
			json.setLength(json.length()-1);
		}
		json.append("]");
		return json.toString();
	}
	
	public static String cellsAdd2json(int detail, Collection<OctreeCell> cells) {
		debug = 0;
		debug2 = 0;
		debug3 = 0;
		debug4 = 0;
		
		if (cells.size() > 0) {
			StringBuffer json = new StringBuffer("[");
			
			for (OctreeCell cell : cells) {
				String s = cell2json(detail, cell);
				if (s != null) {
					json.append(cell2json(detail, cell));
					json.append(",");
				}
			}
			
			if (json.charAt(json.length()-1) == ',') {
				json.setLength(json.length()-1);
			} 
			
			/*
			if (json.charAt(json.length()-1) == '[') {
				return null;
			}
			*/
			
			json.append("]");
			return json.toString();
		} else {
			return "[]";
		}
	}
	
}
