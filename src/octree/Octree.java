package octree;

import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import appearance.ColorGradient;

/**
 * Class representing an octree. 
 * 
 * @author mkoch
 */
public class Octree {
	public OctreeCell root;
	public TreeStatistics statistics = new TreeStatistics();
	
	private int cell_node_limit = 100;
	private int cell_edge_limit = 1000;
	
	/**
	 * Constructs the class from different parameters. The min max values define the geometrical size. The remaining paramters are defined in the properties config file.
	 * 
	 * @param minx
	 * @param maxx
	 * @param miny
	 * @param maxy
	 * @param minz
	 * @param maxz
	 * @param depths
	 * @param colorgradient
	 * @param cell_node_limit
	 * @param cell_edge_limit
	 * @param max_transparency
	 * @param average_transparency
	 */
	public Octree(float minx, float maxx, float miny, float maxy, float minz, float maxz, int depths, ColorGradient colorgradient, int cell_node_limit, int cell_edge_limit, float max_transparency, float average_transparency) {
		
		float sizex = maxx - minx;
		float sizey = maxy - miny;
		float sizez = maxz - minz;
		
		float halfsizex = sizex /2f;
		float halfsizey = sizey /2f;
		float halfsizez = sizez /2f;
		
		this.cell_edge_limit = cell_edge_limit;
		this.cell_node_limit = cell_node_limit;
		
		Vector3f center = new Vector3f(minx + halfsizex, miny + halfsizey, minz + halfsizez);
		this.root = new OctreeCell(null, 0, center, sizex, sizey, sizez, statistics, colorgradient, max_transparency, average_transparency);
		
		
		
		if (depths > 0) {
			expandCell(root, depths);
		}
		
	}
	
	/**
	 * Computes statictics for each cell, using a TreeStatistics object, which also stores the statistics.
	 */
	public void buildTreeStatistics() {
		this.statistics.gatherStatistics(this);
	}
	
	public void calculateVisibleBounds() {
		walkTree(this.root);
	}
	
	private void walkTree(OctreeCell cell) {
		cell.calculateVisibleBound();
		if (cell.children != null) {
			for (OctreeCell child : cell.children ) {
				walkTree(child);
			}
		}
	}
	
	
	/**
	 * Adds children to an octree cell. 
	 * 
	 * @param cell
	 * @param depths
	 */
	private void expandCell(OctreeCell cell, int depths) {
		if (cell.level < depths) {
			float halfsizex = cell.sizex / 2;
			float halfsizey = cell.sizey / 2;
			float halfsizez = cell.sizez / 2;
			float qsx = cell.sizex /4;
			float qsy = cell.sizey /4;
			float qsz = cell.sizez /4;
			
			Vector3f newcenter = new Vector3f(cell.center.x - qsx, cell.center.y - qsy, cell.center.z - qsz);
			OctreeCell cell000 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x + qsx, cell.center.y - qsy, cell.center.z - qsz);
			OctreeCell cell100 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x - qsx, cell.center.y + qsy, cell.center.z - qsz);
			OctreeCell cell010 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x + qsx, cell.center.y + qsy, cell.center.z - qsz);
			OctreeCell cell110 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x - qsx, cell.center.y - qsy, cell.center.z + qsz);
			OctreeCell cell001 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x + qsx, cell.center.y - qsy, cell.center.z + qsz);
			OctreeCell cell101 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x - qsx, cell.center.y + qsy, cell.center.z + qsz);
			OctreeCell cell011 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			newcenter = new Vector3f(cell.center.x + qsx, cell.center.y + qsy, cell.center.z + qsz);
			OctreeCell cell111 = new OctreeCell(cell, cell.level+1, newcenter, halfsizex, halfsizey, halfsizez, statistics, cell.colorgradient, cell.MAX_TRANSPARENCY, cell.AVERAGE_TRANSPARENCY);
			
			cell.children = new OctreeCell[] {cell000, cell001, cell010, cell011, cell100, cell101, cell110, cell111};
			
			for (OctreeCell child : cell.children) {
				expandCell(child, depths);
			}
		}
		
	}
	
	/**
	 * Returns the corresponding octree cell for a coordinate.
	 * 
	 * @param point
	 * @return
	 */
	public OctreeCell getCellForPoint(Vector3f point) {
		OctreeCell cell = this.root;
		while (cell.children != null) {
			int x = (point.x < cell.center.x? 0 : 4);
			int y = (point.y < cell.center.y? 0 : 2);
			int z = (point.z < cell.center.z? 0 : 1);
			cell = cell.children[x+y+z];
		}
		return cell;
	}
	
	/**
	 * Splits a cell. Creates another level for this cell and moves all nodes and edges to its children.
	 * 
	 * @param cell
	 */
	public void splitCell(OctreeCell cell) {
		List<Edge> edges = cell.edges;
		List<Node> nodes = cell.nodes;
		
		expandCell(cell, cell.level+1);
		
		cell.edges = null;
		cell.nodes = null;
		
		// redistribute nodes and edges
		for (Node node : nodes)
			insertNode(node);
		for (Edge edge : edges) 
			insertEdge(edge);
	}
	
	/**
	 * Adds a Node to the tree.
	 * 
	 * @param node
	 */
	public void insertNode(Node node) {
		OctreeCell cell = getCellForPoint(node.center);
		if (this.cell_node_limit > 0 && cell.getNodeCount() >= this.cell_node_limit) {
			// too much nodes in cell: add one level for this cell and add node
			splitCell(cell);
			cell = getCellForPoint(node.center);
		}
		if (node.center.z < (cell.center.z - (cell.sizez/2)) && node.center.z > (cell.center.z + (cell.sizez/2))) System.out.println("ERROR NODE BUILDING:" + node.center.z + " and " + cell.center.z + " - " + cell.sizez); 
		cell.addNode(node);
	}
	
	/**
	 * Adds an Edge to the tree.
	 * 
	 * @param edge
	 */
	public void insertEdge(Edge edge) {
		OctreeCell sourcecell = getCellForPoint(edge.source.center);
		if (this.cell_edge_limit > 0 && sourcecell.getEdgeCount() >= this.cell_edge_limit) {
			// too much nodes in cell: add one level for this cell and add node
			if (sourcecell.getNodeCount() > 1) {
				splitCell(sourcecell);
			}
			sourcecell = getCellForPoint(edge.source.center);
		}
	//	OctreeCell targetcell = getCellForPoint(edge.target.center);
		sourcecell.addEdge(edge);
	}
}
