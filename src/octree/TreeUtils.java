package octree;

/**
 * Utils class to create ids for tree cells.
 * 
 * @author mkoch
 */
public class TreeUtils {
	private static long id_base = 0;
//	private static String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
//	private static String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
 	private static String num2id(long number) {
 		StringBuffer id = new StringBuffer("");
 		while (number > 0) {
 			int i = (int)(number % alphabet.length());
 			id.append(alphabet.charAt(i));
 			number = number / alphabet.length();
 		}
 		return id.toString();
 	}
 	
 	/**
 	 * @return
 	 * Returns a new unique id.
 	 */
 	public static String getNextID() {
 		String id = num2id(id_base);
 		id_base++;
 		return id;
 	}
}
