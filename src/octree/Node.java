package octree;

import java.util.ArrayList;

import javax.vecmath.Vector3f;

/**
 * Class representing an node.
 * 
 * @author mkoch
 */
public class Node {
	public Vector3f center;
	public String id;
	public String name;

	public static float size = 30;
	
	public Node[] targets = null;
	public ArrayList<Node> sources = null;
	
	public Node(Vector3f center, String id, String name, float size) {
		this.size = size;
		this.center = center;
		this.id = id;
		this.name = name;
	}
}
