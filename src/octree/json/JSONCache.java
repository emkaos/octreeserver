package octree.json;

import java.util.HashMap;

import octree.Octree;
import octree.OctreeCell;

/**
 * @author mkoch
 * Unused.
 */
public class JSONCache {
	private static HashMap<OctreeCell, String> d1_cache = new HashMap<OctreeCell, String>();
	private static HashMap<OctreeCell, String> d2_cache = new HashMap<OctreeCell, String>();
	
	private JSONGenerator generator;
	
	public JSONCache(JSONGenerator generator, Octree tree) {
		this.generator = generator;
		walkTree(tree.root);
	}
	
	public void add(OctreeCell cell) {
		d1_cache.put(cell, generator.cell2json(1, cell));
		d2_cache.put(cell, generator.cell2json(2, cell));
	}
	
	public String get_d1(OctreeCell cell) {
		return d1_cache.get(cell);
	}
	
	public String get_d2(OctreeCell cell) {
		return d2_cache.get(cell);
	}
	
	
	public void buildCaches(Octree tree) {
		walkTree(tree.root);
		System.out.println("Finished building JSON Caches");
	}
	
	private void walkTree(OctreeCell cell) {
		add(cell);
		
		if (cell.children != null) {
			for (OctreeCell child : cell.children ) {
				walkTree(child);
			}
		}
	}
}
