package octree.json;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;

import javax.vecmath.Color3f;

import octree.Edge;
import octree.Node;
import octree.Octree;
import octree.OctreeCell;

/**
 * @author mkoch
 *
 */
public class JSONGenerator {
	
	/* FORMAT
	 * DETAIL2:
	 * [[nodeid1, nodex1. nodey1, nodez1, nodeid2, nodec2 ...], [edge1_source_x, edge1_source_y, edge1_source_z, edge1_target_x, edge1_target_y, edge1_target_z, edge2_source_x, ...]]
	 * 
	 * DETAIL3:
	 * [centerx, centery, centerz, sizex, sizey, sizez, color_r, color_g, color_b]
	 */
	
	protected DecimalFormat CF;
	
	protected JSONCache cache;
	
	public JSONGenerator() {
		DecimalFormatSymbols df = new DecimalFormatSymbols(Locale.ENGLISH); // needed because german uses , instead of . and messes up json
		CF = new DecimalFormat("0.#");
	}
	
	/**
	 * Tree parameter was used for caching
	 * @param tree
	 */
	public JSONGenerator(Octree tree) {
	//	cache = new JSONCache(this, tree);
		DecimalFormatSymbols df = new DecimalFormatSymbols(Locale.ENGLISH); // needed because german uses , instead of . and messes up json
		CF = new DecimalFormat("0.#");
	}
	
	/**
	 * Creates a JSON String for an octree cell, depending on the detail level.
	 * 
	 * @param detail
	 * @param cell
	 * @return
	 */
	public String cell2json(int detail, OctreeCell cell) {
		if (cell.getNodeCount() > 0) {
			StringBuilder json = new StringBuilder("[");
			if (detail == 1) {
				json.append("\""+cell.id+"\"");
				json.append(",[");
				//System.out.println(cell.getNodeCount());
				for (Node node : cell.getNodes()) {
					json.append(node.id);
					json.append(',');
					json.append(CF.format(node.center.x));
					json.append(',');
					json.append(CF.format(node.center.y));
					json.append(',');
					json.append(CF.format(node.center.z));
					json.append(',');
				}
				
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append("],[");
				// edges
				
				for (Edge edge : cell.getEdges()) {
					json.append(CF.format(edge.source.center.x));
					json.append(',');
					json.append(CF.format(edge.source.center.y));
					json.append(',');
					json.append(CF.format(edge.source.center.z));
					json.append(',');
					json.append(CF.format(edge.target.center.x));
					json.append(',');
					json.append(CF.format(edge.target.center.y));
					json.append(',');
					json.append(CF.format(edge.target.center.z));
					json.append(',');
				}
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append(']');
			}
			if (detail == 2 ) {
				// nodes
				json.append("\""+cell.id+"\"");
				json.append(",[");
				//System.out.println(cell.getNodeCount());
				for (Node node : cell.getNodes()) {
					json.append(node.id);
					json.append(',');
					json.append(CF.format(node.center.x));
					json.append(',');
					json.append(CF.format(node.center.y));
					json.append(',');
					json.append(CF.format(node.center.z));
					json.append(',');
				}
				
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append("],[");
				// edges
				
				for (Edge edge : cell.getEdges()) {
					json.append(CF.format(edge.source.center.x));
					json.append(',');
					json.append(CF.format(edge.source.center.y));
					json.append(',');
					json.append(CF.format(edge.source.center.z));
					json.append(',');
					json.append(CF.format(edge.target.center.x));
					json.append(',');
					json.append(CF.format(edge.target.center.y));
					json.append(',');
					json.append(CF.format(edge.target.center.z));
					json.append(',');
				}
				if (json.charAt(json.length() -1) == ',') {
					json.setLength(json.length()-1);
				}
				json.append(']');
					
			} else if (detail == 3) {
				Color3f color = cell.getColor();
				json.append("\""+cell.id+"\"");
				json.append(',');
				json.append(CF.format(cell.visible_center.x));
				json.append(',');
				json.append(CF.format(cell.visible_center.y));
				json.append(',');
				json.append(CF.format(cell.visible_center.z));
				json.append(',');
				json.append(CF.format(cell.visible_sizex));
				json.append(',');
				json.append(CF.format(cell.visible_sizey));
				json.append(',');
				json.append(CF.format(cell.visible_sizez));
				json.append(',');
				json.append(color.x);
				json.append(',');
				json.append(color.y);
				json.append(',');
				json.append(color.z);
				json.append(',');
				json.append(cell.getTransparency());
			//	System.out.println(cell.getTransparency());
				//json.append(',');
			}
			
			json.append("]");
			return json.toString();
		} else {
			return null;
		}
	}
	
	/**
	 * Creates a JSON list of octree cells that are going to be deleted. Only the cell ids are included.
	 * 
	 * @param cells
	 * @return
	 */
	public String cellsRemove2json(Collection<OctreeCell> cells) {
		StringBuilder json = new StringBuilder("[");
		for (OctreeCell cell : cells) {
			json.append("\""+cell.id+"\"");
			json.append(",");
		}
		if (json.charAt(json.length() -1) == ',') {
			json.setLength(json.length()-1);
		}
		json.append("]");
		return json.toString();
	}
	
	/**
	 * Creates a JSON list of octree cells that are going to be displayed as debug grids.
	 * @param cells
	 * @return
	 */
	public String cellsDebugGridjson(Collection<OctreeCell> cells) {
		StringBuilder json = new StringBuilder("[");
		
		for (OctreeCell cell : cells) {
				json.append("[");
				json.append(CF.format(cell.visible_center.x));
				json.append(',');
				json.append(CF.format(cell.visible_center.y));
				json.append(',');
				json.append(CF.format(cell.visible_center.z));
				json.append(',');
				json.append(CF.format(cell.visible_sizex));
				json.append(',');
				json.append(CF.format(cell.visible_sizey));
				json.append(',');
				json.append(CF.format(cell.visible_sizez));
				json.append("],");
		}
		if (json.charAt(json.length() -1) == ',') {
			json.setLength(json.length()-1);
		}
		json.append("]");
		return json.toString();
	}
	
	/**
	 * Creates a JSON list representing a Node
	 * @param node
	 * @return
	 */
	public String node2Json(Node node) {
		StringBuffer json = new StringBuffer("[");
		json.append(node.id);
		json.append(',');
		json.append(CF.format(node.center.x));
		json.append(',');
		json.append(CF.format(node.center.y));
		json.append(',');
		json.append(CF.format(node.center.z));
		json.append(']');
		return json.toString();
	}
	
	/**
	 * Creates a JSON list of cells to be added. The representation of the cell depends on the detail level.
	 * @param detail
	 * @param cells
	 * @return
	 */
	public String cellsAdd2json(int detail, Collection<OctreeCell> cells) {
		
		if (cells.size() > 0) {
			StringBuilder json = new StringBuilder("[");
			
			for (OctreeCell cell : cells) {
				String s;
				if (detail == 1) {
					//s = cache.get_d1(cell);
					s = cell2json(detail, cell);
				} else if (detail == 2) {
					//s = cache.get_d2(cell);
					s = cell2json(detail, cell);
				} else {
					s = cell2json(detail, cell);
				}
				if (s != null) {
					json.append(s);
					json.append(",");
				}
			}
			
			if (json.charAt(json.length()-1) == ',') {
				json.setLength(json.length()-1);
			} 
			
			/*
			if (json.charAt(json.length()-1) == '[') {
				return null;
			}
			*/
			
			json.append("]");
			return json.toString();
		} else {
			return "[]";
		}
	}
	
	/**
	 * Creates a JSON list for Nodes to be marked.
	 * @param nodes
	 * @return
	 */
	public String nodesMark2json(Collection<Node> nodes) {
		StringBuilder json = new StringBuilder("[");
		
		for (Node node : nodes) {
			json.append(node2Json(node));
			json.append(",");
		}
		if (json.charAt(json.length() -1) == ',') {
			json.setLength(json.length()-1);
		}
		json.append("]");
		return json.toString();
	}
	
	/**
	 * Creates a JSON list for Nodes to be unmarked.
	 * @param nodes
	 * @return
	 */
	public String nodesUnmark2json(Collection<Node> nodes) {
		StringBuilder json = new StringBuilder("[");
		for (Node node : nodes) {
			json.append("\""+node.id+"\"");
			json.append(",");
		}
		if (json.charAt(json.length() -1) == ',') {
			json.setLength(json.length()-1);
		}
		json.append("]");
		return json.toString();
	}
	
}
