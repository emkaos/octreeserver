package octree;

/**
 *	Class representing an edge.
 *
 * @author matti
 */
public class Edge {
	public Node source;
	public Node target;
	
	/**
	 * @param source
	 * @param target
	 */
	public Edge(Node source, Node target) {
		this.source = source;
		this.target = target;
	}

}
