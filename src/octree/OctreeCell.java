package octree;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Color3f;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

import appearance.ColorGradient;

/**
 * Class representing one octree cell.
 * 
 * @author mkoch
 */
public class OctreeCell {
	public OctreeCell parent = null;
	public OctreeCell[] children = null;
	private TreeStatistics statistics;
	public int level;
	public String id;
	
	List<Node> nodes;
	List<Edge> edges;
	
	public Vector3f center;
	public float sizex;
	public float sizey;
	public float sizez;
	
	public Vector3f visible_center;
	public float visible_sizex;
	public float visible_sizey;
	public float visible_sizez;
	
	//public float maxnodes; // maximal node capacity
	
	public float last_viewing_distance = Float.POSITIVE_INFINITY;
	
	public ColorGradient colorgradient;
 
	public float AVERAGE_TRANSPARENCY = 0.6f; // transparency for average density
	public float MAX_TRANSPARENCY = 0f; // transparency for maximal density
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * Equal test only based on the id.
	 */
	@Override public boolean equals(Object aThat) {
	    if ( this == aThat ) return true;
		if ( !(aThat instanceof OctreeCell) ) return false;
		OctreeCell that = (OctreeCell)aThat;
		return that.id == this.id;
	}
	
	/**
	 * Constructs an OctreeCell from the given parameters.
	 * 
	 * @param parent
	 * @param level
	 * @param center
	 * @param sizex
	 * @param sizey
	 * @param sizez
	 * @param statistics
	 * @param colorgradient
	 * @param max_transparency
	 * @param average_transparency
	 */
	public OctreeCell(OctreeCell parent, int level, Vector3f center, float sizex, float sizey, float sizez, TreeStatistics statistics, ColorGradient colorgradient, float max_transparency, float average_transparency){
		this.AVERAGE_TRANSPARENCY = average_transparency;
		this.MAX_TRANSPARENCY = max_transparency;
		
		nodes = new ArrayList<Node>();
		edges = new ArrayList<Edge>();
		this.statistics = statistics;
		this.parent = parent;
		this.level = level;
		this.center = center;
		this.sizex = sizex;
		this.sizey = sizey;
		this.sizez = sizez;
		this.colorgradient = colorgradient;
		
		/*
		if (parent != null) {
			this.id = parent.id + (int)(Math.pow(10, parent.level)*id);
		} else {
			this.id = 0;
		}
		*/
		this.id = TreeUtils.getNextID();
	//	this.maxnodes = (float)Math.pow(sizex*sizey*sizez, -4)  ; // this should depend on the layout algorithm and the node size 
	};
	
	
	/**
	 * Calculates the visible bounds, e.g. the minimal bounds that include all nodes in the cell.
	 */
	public void calculateVisibleBound() {
		float xmin = Float.POSITIVE_INFINITY;
		float xmax = Float.NEGATIVE_INFINITY;
		float ymin = Float.POSITIVE_INFINITY;
		float ymax = Float.NEGATIVE_INFINITY;
		float zmin = Float.POSITIVE_INFINITY;
		float zmax = Float.NEGATIVE_INFINITY;
		for (Node node : this.getNodes()) {
			if (node.center.x < xmin) xmin = node.center.x;
			if (node.center.y < ymin) ymin = node.center.y;
			if (node.center.z < zmin) zmin = node.center.z;
			if (node.center.x > xmax) xmax = node.center.x;
			if (node.center.y > ymax) ymax = node.center.y;
			if (node.center.z > zmax) zmax = node.center.z;
		}
		
	//	if (zImin < this.center.z - (this.sizez/2)) System.out.println("ALERT: Z inconsistent visible bounds " + zmin + " out of " + (this.center.z - (this.sizez/2)));
	//	if (xmin < this.center.x - (this.sizex/2)) System.out.println("ALERT: X inconsistent visible bounds " + xmin + " out of " + (this.center.x - (this.sizex/2)));
	//	if (ymin < this.center.y - (this.sizey/2)) System.out.println("ALERT: Y inconsistent visible bounds " + ymin + " out of " + (this.center.y - (this.sizey/2)));
		
		this.visible_sizex = (xmax-xmin);
		if (this.visible_sizex < Node.size) this.visible_sizex = Node.size;
		this.visible_sizey = (ymax-ymin);
		if (this.visible_sizey < Node.size) this.visible_sizey = Node.size;
		this.visible_sizez = (zmax-zmin);
		if (this.visible_sizez < Node.size) this.visible_sizez = Node.size;
		this.visible_center = new Vector3f(xmin+(this.visible_sizex/2), ymin+(this.visible_sizey/2), zmin+(this.visible_sizez/2));
		
		//if (this.visible_sizez > this.sizez) System.out.println("Inconsistency " + this.visible_sizez + " " + this.sizez + " " + this.visible_sizex + " " + this.sizex);
	}
	
	/**
	 * Returns the nodecount of the cell. Includes child cells.
	 * 
	 * @return
	 */
	public int getNodeCount() {
		if (this.children != null) {
		  int c = 0;
		  for (OctreeCell child : this.children) {
			 c += child.getNodeCount();
		  }
		  return c;
		} else {
			return nodes.size();
		}
	}
	
	/**
	 * Return the edgecount of the cell. Includes child cells.
	 * 
	 * @return
	 */
	public int getEdgeCount() {
		if (this.children != null) {
			int c = 0;
			for (OctreeCell child : this.children) {
				c += child.getEdgeCount();
			}
			return c;
		} else {
			return edges.size();
		}
	}
	
	/**
	 * Adds a node.
	 * 
	 * @param node
	 */
	public void addNode(Node node) {
		nodes.add(node);
	};

	/**
	 * Adds an edge.
	 * 
	 * @param edge
	 */
	public void addEdge(Edge edge) {
		edges.add(edge);
	};
	
	/**
	 * Returns a list of all Nodes in the cell. Includes all nodes in child cells.
	 * 
	 * @return
	 */
	public List<Node> getNodes() {
		if (this.children == null) {
			return this.nodes;
		} else {
			ArrayList<Node> nodes = new ArrayList<Node>();
		    for (OctreeCell child : this.children) {
		    	nodes.addAll(child.getNodes());
		    }
		    return nodes;
		}
	}
	
	/**
	 * Returns a list of all Edges in the cell. Does not include child cells.
	 * 
	 * @return
	 */
	public List<Edge> getEdges() {
		return this.edges;
	}
	
	/**
	 * Calculates a color for the cell depending on the edge count. Uses the set colorgradient.
	 * 
	 * @return
	 */
	public Color3f getColor() {
		//float g = getNodeCount() / this.statistics.average_node_count;
		float b = getEdgeCount() / this.statistics.average_edge_count;
		
		//s = (b+g)/2f;
		return this.colorgradient.getColor(b);
		//return new Color3f(0f, g, b);
	}
	
	/**
	 * Calculats a trasparency for the cell depending on the node and edge count and set trasparency values in the properties config.
	 * 
	 * @return
	 */
	public float getTransparency() {
		int nodecount = getNodeCount();
		int edgecount = getEdgeCount();
		float node_density = nodecount / (visible_sizex * visible_sizey * visible_sizez);
		float edge_density = edgecount / (visible_sizex * visible_sizey * visible_sizez);
		
		// Max node desity should be 0.2 transparency (nearly solid). Average should be still visible so, 0.6) 
		// calculate for edges and for nodes
		// take the lower value for more visibility
		
		float nv = 0f;
		if (node_density == this.statistics.average_node_density) {
			nv = this.AVERAGE_TRANSPARENCY;
		} else if (node_density < this.statistics.average_node_density) {
			nv =  1 - (1 - this.AVERAGE_TRANSPARENCY) * (node_density - this.statistics.average_node_density) / (0 - this.statistics.average_node_density);
		} else {
			nv = this.MAX_TRANSPARENCY + (this.AVERAGE_TRANSPARENCY - this.MAX_TRANSPARENCY) * (node_density - this.statistics.max_node_density) / (0 - this.statistics.max_node_density);
		}
		
		float ev = 0f;
		if (edge_density == this.statistics.average_edge_density) {
			ev = this.AVERAGE_TRANSPARENCY;
		} else if (edge_density < this.statistics.average_edge_density) {
			ev =  1 - (1 - this.AVERAGE_TRANSPARENCY) * (edge_density - this.statistics.average_edge_density) / (0 - this.statistics.average_edge_density);
		} else {
			ev = this.MAX_TRANSPARENCY + (this.AVERAGE_TRANSPARENCY - this.MAX_TRANSPARENCY) * (edge_density - this.statistics.max_edge_density) / (0 - this.statistics.max_edge_density);
		}
		
		if (ev < nv) 
			return ev;
		else 
			return nv;
	}
}
