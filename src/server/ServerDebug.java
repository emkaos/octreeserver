package server;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/**
 * Helper Class to perform profiling and write server responses to disk.
 * 
 * @author mkoch
 *
 */
public class ServerDebug {

	private static String dir = "/Users/matti/MA/logs";
	
	private static String getNewFileName() {
		return UUID.randomUUID().toString() + ".json";
	}
	
	private static HashMap<String, Long> averages = new HashMap<String, Long>();
	private static HashMap<String, Long> starts = new HashMap<String, Long>();
	private static HashMap<String, Long> counts = new HashMap<String, Long>();
    	
	
	/**
	 * Starts a profiling timer.
	 * 
	 * @param key
	 */
	public static void startTimer(String key) {
		starts.put(key, System.nanoTime());
	}
	
	/**
	 * Stops a profiling timer.
	 * 
	 * @param key
	 */
	public static void stopTimer(String key) {
		long stop = System.nanoTime();
		long diff = stop - starts.get(key);
		if (! counts.containsKey(key)) {
			counts.put(key, 1l);
			averages.put(key, diff);
		} else {
			long count = counts.get(key);
			long average = averages.get(key);
			counts.put(key, count + 1);
			averages.put(key, ((average*count)+diff)/(count+1));
		}
	}
	
	/**
	 * Prints results for all timers to stdout. 
	 */
	public static void timerResults() {
		System.out.println("##### Timing Results #####################");
		for (String key : averages.keySet()) {
			System.out.println(key + ": " + counts.get(key) + " runs with average time " + (averages.get(key)/1000000) + "ms and added time " + (counts.get(key)*averages.get(key)/1000000) + "ms");
		}
		System.out.println("##########################################");
		averages.clear();
		starts.clear();
		counts.clear();
	}
	
	
	/**
	 * Writes a server response to a disk file and prints the path to the file to stdout.
	 * @param s
	 */
	public static void writeFile(String s) {
		String filename = getNewFileName();
		String path = dir + "/" + filename;
		FileWriter fstream;
		try {
			fstream = new FileWriter(path);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(s);
			out.close();
			System.out.println("Wrote response to " + filename);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}
	}
}
