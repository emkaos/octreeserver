package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class TestHandler extends AbstractHandler {
	private String jsondata;
	private String jsondata2;
	
	public TestHandler() {
		// read testfile full of jsons
	    File file = new File("/Users/matti/workspace/octree/testdata/jsontestdata.json");

	    try {
		    byte[] buffer = new byte[(int) file.length()];
		    FileInputStream f = new FileInputStream(file);
		    f.read(buffer);
		    jsondata = new String(buffer);
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }	
	    
	    jsondata2 = "robj = " + jsondata;
	}
	

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        if (target.equals("/1"))
	        response.getWriter().print(jsondata);
        else if (target.equals("/2")) 
	        response.getWriter().print(jsondata2);
	
		
	}

}
