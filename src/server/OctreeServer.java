package server;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import loader.FileLoader;

import octree.Octree;
import octree.culling.OctreeCuller;

import org.eclipse.jetty.server.Server;

import com.sun.security.auth.login.ConfigFile;

import appearance.ColorGradient;

import search.OctreeIndex;
import wikipedia.WikipediaGenerator;
import wikipedia.data.ImageLoader;
import wikipedia.data.ImageProvider;


//http://localhost:8070/pc/0.1232/2.23423/9.23423/1.2/3.4/3423.6

/**
 * HTTP main server
 * 
 * @author mkoch
 *
 */
public class OctreeServer {
	
	/**
	 * Entry point to the program. Reads properties config, creates tree and starts server.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		
		// try to load config
		
		String path = args[0];

	    Properties configFile = new Properties();
	    try {
	    	configFile.load(new FileInputStream(path));
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	System.out.println("ERROR loading config file " + path);
	    	System.exit(-1);
	    }
	    
	    int SERVER_PORT = 0, SCALE = 0, LEVELS = 0, MAX_CELL_NODES = 0, MAX_CELL_EDGES = 0, D1_NODE_CUTOFF = 0, D2_NODE_CUTOFF = 0, D1_EDGE_CUTOFF = 0, D2_EDGE_CUTOFF = 0, D3_CELL_CUTOFF = 0;
	    float NODE_SIZE = 0, VIEW_ANGLE = 0, MAX_TRANSPARENCY = 0, AVERAGE_TRANSPARENCY = 0;
	    String COLOR_GRADIENT_FILE = null, DATA_FILE = null, ADD_DATA = null;
	    boolean DEBUG = false;
	    
	    // set variables
	    try {
	    	SERVER_PORT = Integer.parseInt(configFile.getProperty("SERVER_PORT").trim());
	    	SCALE = Integer.parseInt(configFile.getProperty("SCALE").trim());
	    	LEVELS = Integer.parseInt(configFile.getProperty("TREE_LEVELS").trim());
	    	MAX_CELL_NODES = Integer.parseInt(configFile.getProperty("MAX_CELL_NODES").trim());
	    	MAX_CELL_EDGES = Integer.parseInt(configFile.getProperty("MAX_CELL_EDGES").trim());
	    	VIEW_ANGLE = Float.parseFloat(configFile.getProperty("VIEW_ANLGE").trim());
	    	COLOR_GRADIENT_FILE = configFile.getProperty("COLOR_GRADIENT_FILE").trim();
	    	MAX_TRANSPARENCY = Float.parseFloat(configFile.getProperty("MAX_TRANSPARENCY").trim());
	    	AVERAGE_TRANSPARENCY = Float.parseFloat(configFile.getProperty("AVERAGE_TRANSPARENCY").trim());
	    	NODE_SIZE = Float.parseFloat(configFile.getProperty("NODE_SIZE").trim());
	    	
	    	D1_NODE_CUTOFF = Integer.parseInt(configFile.getProperty("D1_NODE_CUTOFF").trim());
	    	D1_EDGE_CUTOFF = Integer.parseInt(configFile.getProperty("D1_EDGE_CUTOFF").trim());
	    	D2_NODE_CUTOFF = Integer.parseInt(configFile.getProperty("D2_NODE_CUTOFF").trim());
	    	D2_EDGE_CUTOFF = Integer.parseInt(configFile.getProperty("D2_EDGE_CUTOFF").trim());
	    	D3_CELL_CUTOFF = Integer.parseInt(configFile.getProperty("D3_CELL_CUTOFF").trim());
	    	
	    	DEBUG = Boolean.parseBoolean(configFile.getProperty("DEBUG").trim());
	    	
	    	DATA_FILE = configFile.getProperty("DATA_FILE").trim();
	    	ADD_DATA = configFile.getProperty("ADD_DATA");
	    	
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	System.out.println("Error parsing config file " + path);
	    	System.exit(-1);
	    }
	    
		
	    // Create tree
		FileLoader loader = new FileLoader(new File(DATA_FILE));
		ColorGradient gradient = new ColorGradient(new File(COLOR_GRADIENT_FILE));
		
		Octree tree = loader.getTree(LEVELS, SCALE, gradient, MAX_CELL_NODES, MAX_CELL_EDGES, MAX_TRANSPARENCY, AVERAGE_TRANSPARENCY, NODE_SIZE);
		
		// Create index for tree
		OctreeIndex index = new OctreeIndex(tree);
		index.indexOctree();
		System.out.println("Tree search index finished");
		
		// Load additional data
		ImageProvider image_provider;
		if (ADD_DATA != null) {
			ImageLoader image_loader = new ImageLoader(new File(ADD_DATA.trim()));
			image_provider = image_loader.getImageprovider(index);
		} else {
			image_provider = new ImageProvider();
		}
			
		// Initalize Wikipedia specific JSON Generators
		WikipediaGenerator generator = new WikipediaGenerator(tree, image_provider);
		OctreeCuller culler = new OctreeCuller(tree, VIEW_ANGLE, generator, D1_NODE_CUTOFF, D1_EDGE_CUTOFF, D2_NODE_CUTOFF, D2_EDGE_CUTOFF, D3_CELL_CUTOFF, DEBUG);
		
		// Start server
		Server server = new Server(SERVER_PORT);
		server.setHandler(new ServerHandler(culler, index));
		
		server.start();
		server.join();
	}
}
