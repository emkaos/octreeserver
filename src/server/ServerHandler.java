package server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.vecmath.Vector3f;

import octree.Node;
import octree.Octree;
import octree.culling.OctreeCuller;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import path.PathFinder;

import search.OctreeIndex;

/**
 * Class to handle all server reqeusts
 * 
 * @author mkoch
 *
 */
public class ServerHandler extends AbstractHandler {
	
	private OctreeCuller culler;
	private OctreeIndex index;
	
	public ServerHandler(OctreeCuller culler, OctreeIndex index) {
		this.culler = culler;
		this.index = index;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jetty.server.Handler#handle(java.lang.String, org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	/* (non-Javadoc)
	 * @see org.eclipse.jetty.server.Handler#handle(java.lang.String, org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	/* (non-Javadoc)
	 * @see org.eclipse.jetty.server.Handler#handle(java.lang.String, org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	/* (non-Javadoc)
	 * @see org.eclipse.jetty.server.Handler#handle(java.lang.String, org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	/* (non-Javadoc)
	 * @see org.eclipse.jetty.server.Handler#handle(java.lang.String, org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String[] tokens = target.split("/");
		if (tokens.length > 1) {
			
			/**
			 * Update scene request.
			 */
			if (tokens[1].equals("pc") && tokens.length > 6) {
				Vector3f position = new Vector3f(Float.parseFloat(tokens[2]), Float.parseFloat(tokens[3]), Float.parseFloat(tokens[4]));
				Vector3f view_axis = new Vector3f(Float.parseFloat(tokens[5]), Float.parseFloat(tokens[6]), Float.parseFloat(tokens[7]));
				
			//	System.out.println(position + "/" + view_axis);
				long time1= System.nanoTime();
		        String rstring = culler.getVisibleDifferenceJSON(position, view_axis);
				response.setContentLength(rstring.length());
		        long timedelta = System.nanoTime() - time1;
				System.out.println(target + " : " + rstring.length() + " bytes returned in " + (timedelta/1000000) + "ms");
		//		ServerDebug.timerResults();
				//ServerDebug.writeFile(rstring);
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=utf-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Search a single node request. Find nodes by searching the names for the searchstring
			 */
			} else if (tokens[1].equals("sn") && tokens.length > 2) {
				String search_name = tokens[2];
				ArrayList<Node> results = this.index.searchNodes(search_name);
				String rstring = "";
				System.out.println(results.size() + " searchresults found.");
				if (results.size() > 0) {
					StringBuffer rbuffer = new StringBuffer("[");
					for (Node node : results) {
						rbuffer.append("[");
						rbuffer.append("\"");
						rbuffer.append(node.name);
						rbuffer.append("\"");
						rbuffer.append(",");
						rbuffer.append("\"");
						rbuffer.append(node.id);
						rbuffer.append("\"");
						rbuffer.append("],");
					}
					if (rbuffer.charAt(rbuffer.length() -1) == ',') {
						rbuffer.setLength(rbuffer.length()-1);
					}
					rbuffer.append("]");
					rstring = rbuffer.toString();
				} else {
					rstring = "-1";
				}
				response.setCharacterEncoding("UTF-8");
				//response.setContentLength(rstring.length());
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=UTF-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Search a single node by his id.
			 */
			} else if (tokens[1].equals("fn") && tokens.length > 2) {
				String search_name = tokens[2];
				Node result = this.index.findNodeById(search_name);
				String rstring = "";
				if (result != null) {
					rstring = "[" + result.center.x + "," + result.center.y + "," + (result.center.z + result.size*2) + "," + "0,0,1,0]";
				} else {
					rstring = "[]";
				}
				response.setContentLength(rstring.length());
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=utf-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Search a single node by his exact name.
			 */
			} else if (tokens[1].equals("fnn") && tokens.length > 2) {
				String search_name = tokens[2];
				Node result = this.index.findNodeByName(search_name);
				String rstring = "";
				if (result != null) {
					rstring = "[" + result.center.x + "," + result.center.y + "," + (result.center.z + result.size*2) + "," + "0,0,1,0]";
				} else {
					rstring = "[]";
				}
				response.setContentLength(rstring.length());
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=utf-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Get info for node request. Node is given as id.
			 */
			} else if (tokens[1].equals("ni") && tokens.length > 2) {
				String search_id = tokens[2];
				Node result = this.index.findNodeById(search_id);
				String rstring = "";
				if (result != null) {
					rstring = "\"" + result.name + "\"";
				} else {
					rstring = "\"\"";
				}
				//response.setContentLength(rstring.length());
				//System.out.println("Nodeinfo: "+result.id+" : "+result.name);
				response.setCharacterEncoding("UTF-8");
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=UTF-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Find path request.
			 */
			} else if (tokens[1].equals("fp") && tokens.length > 3) {
				String source_id = tokens[2];
				String target_id = tokens[3];
				
				Node from = this.index.findNodeById(source_id);
				Node to = this.index.findNodeById(target_id);
				String rstring = culler.getPath(from, to);
				//System.out.println(rstring);
				//response.setContentLength(rstring.length());
				response.setCharacterEncoding("UTF-8");
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=UTF-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Node neighbors request. (All nodes connected to the given node)
			 */
			} else if (tokens[1].equals("nn") && tokens.length > 2) {
				String node_id = tokens[2];
				
				Node node = this.index.findNodeById(node_id);
				String rstring = culler.getNeighbors(node);
				//System.out.println(rstring);
				//response.setContentLength(rstring.length());
				response.setContentLength(rstring.getBytes("UTF-8").length);
				response.setCharacterEncoding("UTF-8");
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=UTF-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Mark node request.
			 */
			} else if (tokens[1].equals("mn") && tokens.length > 2) {
				String name = tokens[2];
				String rstring;
				Node node = this.index.findNodeByName(name);
				if (node != null) {
					culler.markNode(node);
					rstring = "Found and marked.";
				} else {
					rstring = "Not found.";
				}
				
				//System.out.println(rstring);
				//response.setContentLength(rstring.length());
				response.setContentLength(rstring.getBytes("UTF-8").length);
				response.setCharacterEncoding("UTF-8");
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=UTF-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Unmark node request.
			 */
			} else if (tokens[1].equals("umn") && tokens.length > 2) {
				String name = tokens[2];
				String rstring;
				Node node = this.index.findNodeByName(name);
				if (node != null) {
					culler.unmarkNode(node);
					rstring = "Found and unmarked.";
				} else {
					rstring = "Not found.";
				}
				
				//System.out.println(rstring);
				//response.setContentLength(rstring.length());
				response.setContentLength(rstring.getBytes("UTF-8").length);
				response.setCharacterEncoding("UTF-8");
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=UTF-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			/**
			 * Reset server request.
			 */
			} else if (tokens[1].equals("reset")) {
				culler.reset();
				String rstring = "1";
				System.out.println("Server reset");
				//response.setContentLength(rstring.length());
				response.setCharacterEncoding("UTF-8");
		        response.getWriter().print(rstring);
		        response.setContentType("text/html;charset=utf-8");
		        response.setStatus(HttpServletResponse.SC_OK);
		        baseRequest.setHandled(true);
		        
			} else {
				System.out.println(tokens[2]);
			}
		} else {
	        response.setContentType("text/html;charset=utf-8");
	        response.setStatus(HttpServletResponse.SC_OK);
	        baseRequest.setHandled(true);
	        response.getWriter().println("ERROR");
		}
		
	}

}
