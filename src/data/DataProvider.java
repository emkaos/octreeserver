package data;

import octree.Node;

/**
 * Interface representing a generic data provider, that maps a data value to a node.
 *
 * @author mkoch
 */
public interface DataProvider {
	public String getDataForNode(Node node);
}
